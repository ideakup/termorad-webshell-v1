<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
    <script>
      WebFont.load({
        google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
        active: function() {
            sessionStorage.fonts = true;
        }
      });
    </script>

    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}" />
    <link href="{{ mix('css/appm.css') }}" rel="stylesheet">
</head>
<body>
    
    @yield('content')    
    <!-- Scripts -->
    <script src="{{ mix('js/appm.js') }}"></script>
    <script src="{{ mix('js/resetpass.js') }}"></script>
</body>
</html>