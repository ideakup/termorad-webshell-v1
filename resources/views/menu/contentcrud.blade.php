@extends('layouts.webshell')

@section('content')

	<div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">
                    Menü
                </h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="{{ url('dashboard') }}" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator"> - </li>
                    <li class="m-nav__item">
                        <a href="{{ url('menu/list') }}" class="m-nav__link">
                            <span class="m-nav__link-text">
                                Menü Listesi
                            </span>
                        </a>
                    </li>
                    <li class="m-nav__separator"> - </li>
                    <li class="m-nav__item">
                        <a href="{{ url('menu') }}/{{ Request::segment(2) }}/{{ Request::segment(3) }}" class="m-nav__link">
                            <span class="m-nav__link-text">
                                İçerik Listesi ({{ $menu->variableLang($langs->first()->code)->name }})
                            </span>
                        </a>
                    </li>
                    <li class="m-nav__separator"> - </li>
                    <li class="m-nav__item">
                        <span class="m-nav__link-text">
                            @if (Request::segment(4) == 'add')
                                İçerik Ekle
                            @elseif (Request::segment(4) == 'edit')
                                @if (is_null(Request::segment(6)))
                                    İçerik Detayları
                                @else
                                    İçerik Düzenle
                                @endif
                            @elseif (Request::segment(4) == 'delete')
                                İçerik Sil
                            @elseif (Request::segment(4) == 'galup')
                                Fotoğraf Yükle
                            @endif
                        </span>
                    </li>
                </ul>
            </div>
        </div>
    </div>

	<div class="m-content">
		<div class="m-portlet m-portlet--mobile">
            
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
					<div class="m-portlet__head-title">
						<h3 class="m-portlet__head-text">
							@if (Request::segment(4) == 'add')
                                İçerik Ekle
                            @elseif (Request::segment(4) == 'edit')
                                @if (is_null(Request::segment(6)))
                                    İçerik Detayları
                                @else
                                    İçerik Düzenle
                                @endif
                            @elseif (Request::segment(4) == 'delete')
                                İçerik Sil ({{ $menu->variableLang($langs->first()->code)->name }})
                            @elseif (Request::segment(4) == 'galup')
                                Foto Galeri Düzenle ({{ $menu->variableLang($langs->first()->code)->name }})
                            @endif
						</h3>
					</div>
				</div>
                
				<div class="m-portlet__head-tools">
                    @if (Request::segment(4) == 'edit' && is_null(Request::segment(6)))
                        <a href="{{ url('menu/content') }}/{{ Request::segment(3) }}/delete/{{Request::segment(5)}}" class="btn btn-danger m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill" title="İçerik Sil">
                            <i class="fa fa-trash"></i>
                        </a>
                    @endif
				</div>
			</div>

			<form class="m-form m-form--fit m-form--label-align-right" method="POST" action="{{ url('menu/content/save') }}" id="contentForm">
                
                {{ csrf_field() }}
                <input type="hidden" name="crud" value="{{ Request::segment(4) }}">
                <input type="hidden" name="menu_id" value="{{ Request::segment(3) }}">
                <input type="hidden" name="content_id" value="{{ Request::segment(5) }}">
                <input type="hidden" name="lang" value="{{ Request::segment(6) }}">
                <input type="hidden" id="formdata" name="formdata"> 
                <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">

                <div class="m-portlet__body">
                    
                    <div class="form-group m-form__group row">
                        <div class="col-10 ml-auto">
                            <h3 class="m-form__section">
                                @if (!is_null(Request::segment(6))) 
                                    [{{ Request::segment(6) }}] 
                                @endif

                                @if (Request::segment(4) == 'add')
                                    İçerik Ekle
                                @elseif (Request::segment(4) == 'edit')
                                    @if (is_null(Request::segment(6)))
                                        İçerik Detayları
                                    @else
                                        İçerik Düzenle
                                    @endif
                                @elseif (Request::segment(4) == 'delete')
                                    İçerik Sil ({{ $menu->variableLang($langs->first()->code)->name }})
                                @elseif (Request::segment(4) == 'galup')
                                    Galeri Düzenle
                                @endif

                                @if (Request::segment(4) != 'add')
                                    <small> (
                                        @if ($content->type == 'text')
                                            Metin & HTML
                                        @elseif ($content->type == 'photo')
                                            Fotoğraf
                                        @elseif ($content->type == 'photogallery')
                                            Foto Galeri
                                        @elseif ($content->type == 'link')
                                            Button & Link
                                        @elseif ($content->type == 'form')
                                            Form
                                        @endif
                                    ) </small>
                                @endif
                            </h3>
                        </div>
                    </div>

                    <div class="form-group m-form__group row @if ($errors->has('title')) has-danger @endif">
                        <label for="example-text-input" class="col-2 col-form-label">
                            Başlık
                        </label>
                        <div class="col-7">
                            <input class="form-control m-input" type="text" id="title" name="title" 
                                @if (Request::segment(4) != 'add' && empty(old('title'))) 
                                    value="@if(is_null($content->variableLang(Request::segment(6)))){{ $content->variableLang($langs->first()->code)->title }}@else{{ $content->variableLang(Request::segment(6))->title }}@endif"
                                @else 
                                    value="{{ old('title') }}" 
                                @endif
                                
                                @if (Request::segment(4) == 'delete' || (Request::segment(4) != 'add' && is_null($content->variableLang(Request::segment(6)))))
                                    disabled="disabled" 
                                @endif
                            required autofocus>
                            @if ($errors->has('title'))
                                <div id="title-error" class="form-control-feedback">{{ $errors->first('title') }}</div>
                            @endif
                        </div>
                    </div>
                    
                    @if (Request::segment(4) == 'add')
                        <div class="form-group m-form__group row @if ($errors->has('type')) has-danger @endif">
                            <label for="example-text-input" class="col-2 col-form-label">
                                Tip
                            </label>
                            <div class="col-7">
                                @if ($errors->has('type'))
                                    <div id="type-error" class="form-control-feedback">{{ $errors->first('type') }}</div>
                                @endif
                                <select class="form-control m-select2" id="type" name="type"
                                    @if (Request::segment(4) == 'delete')
                                        disabled="disabled" 
                                    @endif
                                >
                                    <option value="null">Seçiniz...</option>
                                    <option value="text"> Metin & HTML </option>
                                    <option value="photo"> Fotoğraf </option>
                                    <option value="photogallery"> Foto Galeri </option>
                                    <option value="link"> Button & Link </option>
                                    <option value="form"> Form </option>
                                    <!--
                                        <option value="youtube"> Video (Youtube) </option>
                                        <option value="vimeo"> Video (Vimeo) </option>
                                        <option value="file"> Dosya </option>
                                        <option value="audio"> Ses </option>
                                        <option value="code"> Kod </option>
                                        <option value="slide"> Slide </option>
                                    -->
                                </select>
                            </div>
                        </div>
                    @endif

                    @if (Request::segment(4) == 'add' || (Request::segment(4) == 'edit' && is_null(Request::segment(6))))
                        
                        <div class="form-group m-form__group row @if ($errors->has('order')) has-danger @endif">
                            <label for="example-text-input" class="col-2 col-form-label">
                                Sıralama
                            </label>
                            <div class="col-7">
                                <input class="form-control m-input" type="number" min="1" max="100000" id="order" name="order" 
                                    @if (Request::segment(4) != 'add' && empty(old('order'))) 
                                        value="{{ $content->order }}"
                                    @else 
                                        value="{{ old('order') }}" 
                                    @endif

                                    @if (Request::segment(4) == 'delete')
                                        disabled="disabled" 
                                    @endif
                                required autofocus>
                                @if ($errors->has('order'))
                                    <div id="order-error" class="form-control-feedback">{{ $errors->first('order') }}</div>
                                @endif
                            </div>
                        </div>

                        <div class="m-form__group form-group row">
                            <label for="example-text-input" class="col-2 col-form-label">
                                Pasif / Aktif
                            </label>
                            <div class="col-3">
                                <span class="m-switch">
                                    <label>
                                        <input type="checkbox" @if (Request::segment(4) == 'add' || $content->status == 'active') {{ 'checked="checked"' }} @endif  id="status" name="status" value="active" 
                                            @if (Request::segment(4) == 'delete')
                                                disabled="disabled" 
                                            @endif
                                        />
                                        <span></span>
                                    </label>
                                </span>
                            </div>
                        </div>
                    @endif

                    @if (Request::segment(4) == 'edit' && is_null(Request::segment(6)))
                        @if ($content->type == 'text' || $content->type == 'photogallery')
                            <div class="form-group m-form__group row @if ($errors->has('tag')) has-danger @endif">
                                <label for="example-text-input" class="col-2 col-form-label">
                                    Etiket
                                </label>
                                <div class="col-7">
                                    @if ($errors->has('tag'))
                                        <div id="tag-error" class="form-control-feedback">{{ $errors->first('tag') }}</div>
                                    @endif
                                    <select class="form-control m-select2" id="tag" name="tag[]" multiple="multiple"
                                        @if (Request::segment(4) == 'delete')
                                            disabled="disabled" 
                                        @endif
                                    >
                                        <option value="null">Seçiniz...</option>

                                        @foreach ($tags as $tag)
                                            <option value="{{ $tag->id }}" @if ($content->tags()->where('tag_id', $tag->id)->count()) {{ 'selected' }} @endif> {{ $tag->variableLang($langs->first()->code)->title }} </option>
                                        @endforeach
                                        
                                    </select>
                                </div>
                            </div>

                            <div class="form-group m-form__group row @if ($errors->has('category')) has-danger @endif">
                                <label for="example-text-input" class="col-2 col-form-label">
                                    Kategori
                                </label>
                                <div class="col-7">
                                    @if ($errors->has('category'))
                                        <div id="category-error" class="form-control-feedback">{{ $errors->first('category') }}</div>
                                    @endif
                                    <select class="form-control m-select2" id="category" name="category[]" multiple="multiple"
                                        @if (Request::segment(4) == 'delete')
                                            disabled="disabled" 
                                        @endif
                                        >
                                        <option value="null">Seçiniz...</option>

                                        @foreach ($categories as $category)
                                            <option value="{{ $category->id }}" @if ($content->categories()->where('category_id', $category->id)->count()) {{ 'selected' }} @endif> {{ $category->variableLang($langs->first()->code)->title }} </option>
                                        @endforeach
                                        
                                    </select>
                                </div>
                            </div>
                        @endif

                        @if ($content->type == 'photo')
                            <hr>
                            <div class="form-group m-form__group row @if ($errors->has('props')) has-danger @endif">
                                <label for="example-text-input" class="col-2 col-form-label">
                                    Gösterim Türü
                                </label>
                                <div class="col-7">
                                    @if ($errors->has('props'))
                                        <div id="props-error" class="form-control-feedback">{{ $errors->first('props') }}</div>
                                    @endif
                                    <select class="form-control m-select2" id="props" name="props"
                                        @if (Request::segment(4) == 'delete')
                                            disabled="disabled" 
                                        @endif
                                    >
                                        <option value="responsive" @if (!empty($content) && $content->variableLang($langs->first()->code)->props == 'responsive') {{ 'selected' }} @endif> Responsive </option>
                                        <option value="fixed" @if (!empty($content) && $content->variableLang($langs->first()->code)->props == 'fixed') {{ 'selected' }} @endif> Gerçek Boyut </option>
                                    </select>
                                </div>
                            </div>
                        @endif

                        @if ($content->type == 'photogallery')
                            <hr>
                            <div class="form-group m-form__group row @if ($errors->has('props')) has-danger @endif">
                                <label for="example-text-input" class="col-2 col-form-label">
                                    Kolon Sayısı
                                </label>
                                <div class="col-7">
                                    @if ($errors->has('props'))
                                        <div id="props-error" class="form-control-feedback">{{ $errors->first('props') }}</div>
                                    @endif
                                    <select class="form-control m-select2" id="props" name="props"
                                        @if (Request::segment(4) == 'delete')
                                            disabled="disabled" 
                                        @endif
                                    >
                                        <option value="col-2x" @if (!empty($content) && $content->variableLang($langs->first()->code)->props == 'col-2x') {{ 'selected' }} @endif> 2 Kolon </option>
                                        <option value="col-3x" @if (!empty($content) && $content->variableLang($langs->first()->code)->props == 'col-3x') {{ 'selected' }} @endif> 3 Kolon </option>
                                        <option value="col-4x" @if (!empty($content) && $content->variableLang($langs->first()->code)->props == 'col-4x') {{ 'selected' }} @endif> 4 Kolon </option>
                                    </select>
                                </div>
                            </div>
                        @endif
                    @endif

                    @if (Request::segment(4) == 'edit' && !is_null(Request::segment(6)))
                        
                        @if ($content->type == 'text')
                            <hr>
                            <div class="form-group m-form__group row @if ($errors->has('short_content')) has-danger @endif">
                                <label for="example-text-input" class="col-2 col-form-label">
                                    Kısa İçerik
                                </label>
                                <div class="col-7">
                                    <input class="form-control m-input" type="text" id="short_content" name="short_content" 
                                        @if (Request::segment(4) != 'add' && empty(old('short_content'))) 
                                            value="@if(is_null($content->variableLang(Request::segment(6)))){{ $content->variableLang($langs->first()->code)->short_content }}@else{{ $content->variableLang(Request::segment(6))->short_content }}@endif"
                                        @else 
                                            value="{{ old('short_content') }}" 
                                        @endif
                                        
                                        @if (Request::segment(4) == 'delete' || (Request::segment(4) != 'add' && is_null($content->variableLang(Request::segment(6)))))
                                            disabled="disabled" 
                                        @endif
                                    required autofocus>
                                    @if ($errors->has('short_content'))
                                        <div id="short_content-error" class="form-control-feedback">{{ $errors->first('short_content') }}</div>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group m-form__group" style="padding-bottom: 0;">
                                <label>İçerik</label>
                            </div>

                            <div class="form-group m-form__group @if ($errors->has('content')) has-danger @endif">
                                <textarea class="form-control m-input" id="content" name="content" rows="3">@if (Request::segment(4) != 'add' && empty(old('content'))) @if (is_null($content->variableLang(Request::segment(6)))) {{ $content->variableLang($langs->first()->code)->content }} @else {{ $content->variableLang(Request::segment(6))->content }} @endif @else {{ old('content') }} @endif</textarea>

                                @if ($errors->has('content'))
                                    <div id="content-error" class="form-control-feedback">{{ $errors->first('content') }}</div>
                                @endif
                            </div>
                            
                            <hr>
                            <div class="form-group m-form__group row">
                                <div class="col-10 ml-auto">
                                    <h3 class="m-form__section">
                                        Fon Görseli
                                    </h3>
                                </div>
                            </div>

                            <div class="form-group m-form__group row @if ($errors->has('row')) has-danger @endif">
                                <label for="example-text-input" class="col-2 col-form-label">
                                    Satır Boyutu
                                </label>
                                <div class="col-7">
                                    @if ($errors->has('row'))
                                        <div id="row-error" class="form-control-feedback">{{ $errors->first('row') }}</div>
                                    @endif
                                    <select class="form-control m-select2" id="row" name="row"
                                        @if (Request::segment(4) == 'delete')
                                            disabled="disabled" 
                                        @endif
                                    >
                                        <option value="normal" @if ($content->variableLang(Request::segment(6))->row == old('row') || $content->variableLang(Request::segment(6))->row == 'normal') {{ 'selected' }} @endif> Normal </option>
                                        <option value="full" @if ($content->variableLang(Request::segment(6))->row == old('row') || $content->variableLang(Request::segment(6))->row == 'full') {{ 'selected' }} @endif> Full </option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group m-form__group row @if ($errors->has('height')) has-danger @endif">
                                <label for="example-text-input" class="col-2 col-form-label">
                                    Satır Yüksekliği (px)
                                </label>
                                <div class="col-7">
                                    <input class="form-control m-input" type="number" min="1" max="1000" id="height" name="height" 
                                        @if (Request::segment(4) != 'add' && empty(old('height'))) 
                                            value="@if(is_null($content->variableLang(Request::segment(6)))){{ $content->variableLang($langs->first()->code)->height }}@else{{ $content->variableLang(Request::segment(6))->height }}@endif"
                                        @else 
                                            value="{{ old('height') }}" 
                                        @endif

                                        @if (Request::segment(4) == 'delete')
                                            disabled="disabled" 
                                        @endif
                                    >
                                    @if ($errors->has('height'))
                                        <div id="height-error" class="form-control-feedback">{{ $errors->first('height') }}</div>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group m-form__group row">
                                <div class="col-3">
                                    <div class="m-dropzone dropzone" action="{{ url('/uploadFile') }}" id="dropzone-bg-file-upload">
                                        <div class="m-dropzone__msg dz-message needsclick">
                                            <h3 class="m-dropzone__msg-title">
                                                Dosya yüklemek için dosyayı buraya sürükleyin yada bu alan tıklayın.
                                            </h3>
                                            <span class="m-dropzone__msg-desc">
                                                Bu alan <strong>jpg</strong> ve <strong>png</strong> formatlarını yüklemenize izin verir.
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-9">
                                    <div class="col-12" id="bgImgContainer">
                                        @if (!is_null($content->variableLang(Request::segment(6))) && !is_null($content->variableLang(Request::segment(6))->bgimageurl))
                                            <img class="img-fluid" src="{{ url('upload/xlarge/'.$content->variableLang(Request::segment(6))->bgimageurl) }}" />
                                        @else
                                            <div class="m-alert m-alert--outline alert alert-warning alert-dismissible fade show" role="alert">
                                                <strong>Uyarı!</strong> Yüklenmiş Görsel Bulunmuyor...
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        @elseif ($content->type == 'photo')
                            <hr>
                            <div class="form-group m-form__group row">
                                <div class="col-6">
                                    <div class="m-dropzone dropzone" action="{{ url('/uploadFile') }}" id="dropzone-file-upload">
                                        <div class="m-dropzone__msg dz-message needsclick">
                                            <h3 class="m-dropzone__msg-title">
                                                Dosya yüklemek için dosyayı buraya sürükleyin yada bu alan tıklayın.
                                            </h3>
                                            <span class="m-dropzone__msg-desc">
                                                Bu alan <strong>jpg</strong> ve <strong>png</strong> formatlarını yüklemenize izin verir.
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-6">
                                    <div class="col-12" id="imgContainer">
                                        @if (!is_null($content->variableLang(Request::segment(6))) && !empty($content->variableLang(Request::segment(6))->content))
                                            <img class="img-fluid" src="{{ url('upload/xlarge/'.$content->variableLang(Request::segment(6))->content) }}" />
                                        @else
                                            <div class="m-alert m-alert--outline alert alert-warning alert-dismissible fade show" role="alert">
                                                <strong>Uyarı!</strong> Yüklenmiş Görsel Bulunmuyor...
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        @elseif ($content->type == 'link')
                            <hr>
                            <div class="form-group m-form__group row @if ($errors->has('content')) has-danger @endif">
                                <label for="example-text-input" class="col-2 col-form-label">
                                    Link
                                </label>
                                <div class="col-7">
                                    <input class="form-control m-input" type="text" id="content" name="content" 
                                        @if (Request::segment(4) != 'add' && empty(old('content'))) 
                                            value="{{ $content->variableLang(Request::segment(6))->content }}"
                                        @else 
                                            value="{{ old('content') }}" 
                                        @endif

                                        @if (Request::segment(4) == 'delete')
                                            disabled="disabled" 
                                        @endif
                                    required autofocus>
                                    @if ($errors->has('content'))
                                        <div id="content-error" class="form-control-feedback">{{ $errors->first('content') }}</div>
                                    @endif
                                </div>
                            </div>

                            <div class="m-form__group form-group row">
                                <label for="example-text-input" class="col-2 col-form-label">
                                    Dahili / Dış Link
                                </label>
                                <div class="col-3">
                                    <span class="m-switch">
                                        <label>
                                            <input type="checkbox" @if ($content->variableLang(Request::segment(6))->props == 'external' || Request::segment(4) == 'add') {{ 'checked="checked"' }} @endif id="props" name="props" value="external" 
                                                @if (Request::segment(4) == 'delete')
                                                    disabled="disabled" 
                                                @endif
                                            />
                                            <span></span>
                                        </label>
                                    </span>
                                </div>
                            </div>
                        @endif
                    @endif

                </div>

                <div class="m-portlet__foot m-portlet__foot--fit">
                    <div class="m-form__actions">
                        <div class="row">
                            <div class="col-2"></div>
                            <div class="col-7">
                                @if (Request::segment(4) == 'add' || Request::segment(4) == 'edit')
                                    <button class="btn btn-accent m-btn m-btn--air m-btn--custom" id="postBtn">
                                        Kaydet
                                    </button>
                                    &nbsp;&nbsp;
                                    <a href="{{ url('menu/content') }}/{{ Request::segment(3) }}" class="btn btn-secondary m-btn m-btn--air m-btn--custom">
                                        Vazgeç
                                    </a>
                                @elseif (Request::segment(4) == 'delete')
                                    <div class="alert alert-danger" role="alert">
                                        <strong> Siliyorsunuz... </strong>
                                        Bu işlem geri alınamaz ve ilişkili kayıtlarda soruna sebep olabilir.
                                    </div>
                                    <button class="btn btn-danger m-btn m-btn--air m-btn--custom" id="postBtn">
                                        Kaydı Sil
                                    </button>
                                    &nbsp;&nbsp;
                                    <a href="{{ url('menu/content') }}/{{ Request::segment(3) }}" class="btn btn-secondary m-btn m-btn--air m-btn--custom">
                                        Vazgeç
                                    </a>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>

            </form>

            @if (Request::segment(4) == 'edit' && !is_null(Request::segment(6)))
                @if ($content->type == 'form')
                    <div class="m-portlet m-portlet--mobile">
                        <div class="m-portlet__body">
                            <div class="form-group m-form__group row">
                                <div class="col-12">
                                    <div id="fb-editor"></div>
                                    <style type="text/css">
                                        #fb-editor {
                                            background: #f5f5f5;
                                            padding: 5px;
                                            border-radius: 5px;
                                            -webkit-box-shadow: 1px 1px 5px 0px rgba(0,0,0,0.75);
                                            -moz-box-shadow: 1px 1px 5px 0px rgba(0,0,0,0.75);
                                            box-shadow: 1px 1px 5px 0px rgba(0,0,0,0.75);

                                        }
                                        #fb-editor [class^="icon-"] {
                                            display: inherit;
                                        }
                                        .form-builder-dialog code {
                                            border: 0;
                                            box-shadow: none;
                                        }

                                        #fb-editor .form-elements>.className-wrap,
                                        #fb-editor .form-elements>.placeholder-wrap,
                                        #fb-editor .form-elements>.access-wrap {
                                            display: none !important;
                                        }
                                    </style>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
            @endif

		</div>
    
        @if (Request::segment(4) == 'galup' && $content->type == 'photogallery')
            <div class="m-portlet m-portlet--mobile">

                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">
                                Fotoğraf Yükle
                            </h3>
                        </div>
                    </div>
                </div>

                <div class="m-portlet__body">
                    <div class="form-group m-form__group row" id="imgContainer">

                        <div class="col-12">
                            <div class="m-dropzone dropzone" action="{{ url('/uploadFile') }}" id="dropzone-file-upload">
                                <div class="m-dropzone__msg dz-message needsclick">
                                    <h3 class="m-dropzone__msg-title">
                                        Dosya yüklemek için dosyayı buraya sürükleyin yada bu alan tıklayın.
                                    </h3>
                                    <span class="m-dropzone__msg-desc">
                                        Bu alan <strong>jpg</strong> ve <strong>png</strong> formatlarını yüklemenize izin verir.
                                    </span>
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-12">
                            <div class="col-12">
                                @if ($content->photogallery->count() == 0)
                                    <div class="m-alert m-alert--outline alert alert-warning alert-dismissible fade show" role="alert">
                                        <strong>Uyarı!</strong> Yüklenmiş Görsel Bulunmuyor...
                                    </div>
                                @endif
                                <p> </p>
                            </div>
                        </div>

                        @foreach ($content->photogallery as $img)
                            <div class="col-xl-4">
                                <div class="m-portlet m-portlet--bordered-semi m-portlet--full-height" style="margin-bottom: 0;">
                                    <div class="m-portlet__head m-portlet__head--fit">
                                        <div class="m-portlet__head-tools">
                                            <ul class="m-portlet__nav">
                                                <li class="m-portlet__nav-item" aria-expanded="true">
                                                    <a href="{{ url('menu/content/'.Request::segment(3).'/edit/'.Request::segment(5).'/photogallery_edit/'.$img->id) }}" class="m-portlet__nav-link btn btn-secondary m-btn m-btn--air m-btn--icon m-btn--icon-only m-btn--pill">
                                                        <i class="fa fa-cog"></i>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="m-portlet__body">
                                        <div class="m-widget19">
                                            <div class="m-widget19__pic m-portlet-fit--top m-portlet-fit--sides">
                                                <img class="img-fluid" src="{{ url('upload/thumbnail3x/'.$img->url) }}" />
                                                <p class="m-widget19__title m--font-light" style="padding-bottom: 0;">
                                                    {{ $img->name }}
                                                </p>
                                                <div class="m-widget19__shadow" style="background: linear-gradient(rgba(0, 0, 0, 0), rgba(0, 0, 0, 1) 60%);"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>

            </div>
        @endif

	</div>

@endsection


@section('inline-scripts')

@if (Request::segment(4) == 'edit' && !is_null(Request::segment(6)))
    @if ($content->type == 'form')
        <script src="{{ asset('js/formBuilder.js') }}"></script>
    @endif
@endif

<script type="text/javascript">
    
    @if (Request::segment(4) == 'galup' && is_null(Request::segment(6)))
        @if ($content->type == 'photogallery')
            Dropzone.options.dropzoneFileUpload = {
                headers: { 'X-CSRF-TOKEN': $('#token').val() },
                paramName: "file", // The name that will be used to transfer the file
                maxFiles: 5,
                maxFilesize: 5, // MB
                addRemoveLinks: true,
                accept: function(file, done) {
                    done();
                },
                init: function () {
                    //console.log('dropzone init');
                },
                sending: function(file, xhr, formData){
                    formData.append('uploadType', 'galleryimage');
                    formData.append('contentid', {{ $content->id }});
                },
                success: function(file, xhr, event){
                    var data = jQuery.parseJSON(xhr);
                    $("#imgContainer").append('<div class="col-xl-4">'+
                    '    <div class="m-portlet m-portlet--bordered-semi m-portlet--full-height" style="margin-bottom: 0;">'+
                    '        <div class="m-portlet__head m-portlet__head--fit">'+
                    '            <div class="m-portlet__head-tools">'+
                    '                <ul class="m-portlet__nav">'+
                    '                    <li class="m-portlet__nav-item" aria-expanded="true">'+
                    '                        <a href="{{ url('menu/content/'.Request::segment(3).'/edit/'.Request::segment(5).'/photogallery_edit/') }}/' + data.id + '" class="m-portlet__nav-link btn btn-secondary m-btn m-btn--air m-btn--icon m-btn--icon-only m-btn--pill">'+
                    '                           <i class="fa fa-cog"></i>'+
                    '                        </a>'+
                    '                    </li>'+
                    '                </ul>'+
                    '            </div>'+
                    '        </div>'+
                    '        <div class="m-portlet__body">'+
                    '            <div class="m-widget19">'+
                    '                <div class="m-widget19__pic m-portlet-fit--top m-portlet-fit--sides">'+
                    '                    <img class="img-fluid" src="{{ url('upload/thumbnail3x/') }}/' + data.location + '" />'+
                    '                    <p class="m-widget19__title m--font-light" style="padding-bottom: 0;">'+
                    '                    </p>'+
                    '                    <div class="m-widget19__shadow" style="background: linear-gradient(rgba(0, 0, 0, 0), rgba(0, 0, 0, 1) 60%);"></div>'+
                    '               </div>'+
                    '           </div>'+
                    '        </div>'+
                    '    </div>'+
                    '</div>');
                    this.removeFile(file);
                }
            };
        @endif
    @endif

    @if ((Request::segment(4) == 'edit' || Request::segment(4) == 'galup') && !is_null(Request::segment(6)))
        @if ($content->type == 'text')
            tinymce.init({
                selector: 'textarea#content',

                @if (Request::segment(4) == 'delete')
                    readonly : 1,
                @endif
                
                //plugins: 'print preview fullpage searchreplace autolink directionality visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists wordcount imagetools textpattern help',
                //Color Picker, Context Menu, MoxieManager, noneditable, pageembed,  Text Color documentation
                //, linkchecker TEKRAR BAK
                //codesample, directionality, fullpage, help, pagebreak, paste, print, template, textpattern, toc, visualchars

                plugins: 'advlist lists anchor autolink autoresize charmap code fullscreen hr image imagetools insertdatetime link media nonbreaking preview searchreplace table visualblocks wordcount importcss paste',
                
                // table tabledelete | tableprops tablerowprops tablecellprops | tableinsertrowbefore tableinsertrowafter tabledeleterow | tableinsertcolbefore tableinsertcolafter tabledeletecol
                // formatselect styleselect fontselect fontsizeselect
                toolbar: 'undo redo | formatselect | bold italic underline strikethrough subscript superscript | removeformat | forecolor backcolor | alignleft aligncenter alignright alignjustify alignnone | numlist bullist outdent indent | link image media | anchor charmap hr insertdatetime nonbreaking | searchreplace | visualblocks code fullscreen',
                imagetools_toolbar: "rotateleft rotateright | flipv fliph | editimage imageoptions",

                branding: false,
                min_height: 400,

                relative_urls: false,
                convert_urls: false,

                /* without images_upload_url set, Upload tab won't show up*/
                images_upload_url: '{{ url('/uploadFile') }}',
                images_upload_base_path: '{{ url('/') }}',
                images_upload_credentials: true,
                images_upload_handler: function (blobInfo, success, failure) {
                   var xhr, formData;
                   xhr = new XMLHttpRequest();
                   xhr.withCredentials = false;
                   xhr.open('POST', '{{ url('/uploadFile') }}');
                   var token = '{{ csrf_token() }}';
                   xhr.setRequestHeader("X-CSRF-Token", token);
                   xhr.onload = function() {
                       var json;
                       if (xhr.status != 200) {
                           failure('HTTP Error: ' + xhr.status);
                           return;
                       }
                       json = JSON.parse(xhr.responseText);

                       if (!json || typeof json.location != 'string') {
                           failure('Invalid JSON: ' + xhr.responseText);
                           return;
                       }
                       success(json.location);
                   };
                   formData = new FormData();
                   formData.append('file', blobInfo.blob(), blobInfo.filename());
                   formData.append('uploadType', 'editor');
                   xhr.send(formData);
               }
            });

            Dropzone.options.dropzoneBgFileUpload = {
                headers: { 'X-CSRF-TOKEN': $('#token').val() },
                paramName: "file", // The name that will be used to transfer the file
                maxFiles: 1,
                maxFilesize: 5, // MB
                addRemoveLinks: true,
                accept: function(file, done) {
                    done();
                },
                init: function () {
                    //console.log('dropzone init');
                },
                sending: function(file, xhr, formData){
                    formData.append('uploadType', 'contentbgimage');
                    formData.append('contentid', {{ $content->id }});
                    formData.append('lang_code', '{{ Request::segment(6) }}');
                },
                success: function(file, xhr, event){
                    var data = jQuery.parseJSON(xhr);
                    $("#bgImgContainer").html('<img class="img-fluid" src="{{ url('') }}/upload/xlarge/' + data.location + '" />');
                    this.removeFile(file);
                }
            };
        @endif

        @if ($content->type == 'photo')
            Dropzone.options.dropzoneFileUpload = {
                headers: { 'X-CSRF-TOKEN': $('#token').val() },
                paramName: "file", // The name that will be used to transfer the file
                maxFiles: 1,
                maxFilesize: 5, // MB
                addRemoveLinks: true,
                accept: function(file, done) {
                    done();
                },
                init: function () {
                    //console.log('dropzone init');
                },
                sending: function(file, xhr, formData){
                    formData.append('uploadType', 'contentimage');
                    formData.append('contentid', {{ $content->id }});
                    formData.append('lang_code', '{{ Request::segment(6) }}');
                },
                success: function(file, xhr, event){
                    var data = jQuery.parseJSON(xhr);
                    $("#imgContainer").html('<img class="img-fluid" src="{{ url('') }}/upload/xlarge/' + data.location + '" />');
                    this.removeFile(file);
                }
            };
        @endif

        @if ($content->type == 'form')
            var options = {
                i18n: {
                    locale: 'tr-TR'
                },
                formData: @if(is_null($content->variableLang(Request::segment(6)))) JSON.stringify({!! $content->variableLang($langs->first()->code)->content!!}) @else JSON.stringify({!! $content->variableLang(Request::segment(6))->content!!}) @endif,
                dataType: 'json',
                editOnAdd: true,
                disableFields: ['button', 'autocomplete', 'hidden', 'file'],
                showActionButtons: false,
                sortableControls: true,
                controlPosition: 'left',
                stickyControls: {
                    enable: false
                },
                controlOrder: [
                    'header',
                    'paragraph',
                    'text',
                    'textarea',
                    'date',
                    'number',
                    'checkbox-group',
                    'radio-group',
                    'select'
                ],
                disabledAttrs: [
                    'inline',
                    'maxlength',
                    'multiple',
                    'name',
                    'other',
                    'rows',
                    'step',
                    'toggle',
                    'value'
                ],
                disabledSubtypes: {
                    paragraph: ['address', 'blockquote', 'canvas', 'output'],
                    text: ['password', 'color'],
                    textarea: ['tinymce', 'quill'],
                },
                replaceFields: [
                    {
                        type: "text",
                        label: "Metin",
                        maxlength: 250
                    },
                    {
                        type: "textarea",
                        label: "Metin Alanı",
                        rows: 4,
                        maxlength: 1000
                    },
                    {
                        type: "number",
                        label: "Numara",
                        step: 1
                    },
                    {
                        type: "checkbox-group",
                        label: "Onay Kutusu Grubu",
                        inline: true
                    },
                    {
                        type: "radio-group",
                        label: "Radyo Grubu",
                        inline: true
                    }
                ]
            };
        @endif
    @endif

    $(document).ready(function(){

        @if (Request::segment(4) == 'add')
            $('#type').select2({
                placeholder: "Seçiniz..."
            });
        @endif
        
        @if (Request::segment(4) == 'edit' && !is_null(Request::segment(6)))
            @if ($content->type == 'text')
                $('#row').select2({
                    placeholder: "Seçiniz..."
                });
            @endif
            @if ($content->type == 'form')
                var formBuilder = $('#fb-editor').formBuilder(options);
            @endif
        @endif

        @if (Request::segment(4) == 'edit' && is_null(Request::segment(6)))
            @if ($content->type == 'text' || $content->type == 'photogallery')
                $('#tag').select2({
                    placeholder: "Seçiniz..."
                });
                $('#category').select2({
                    placeholder: "Seçiniz..."
                });
            @endif

            @if ($content->type == 'photo' || $content->type == 'photogallery')
                $('#props').select2({
                    placeholder: "Seçiniz..."
                });
            @endif
        @endif

        $('#postBtn').click(function(e) {

            e.preventDefault();
            var btn = $(this);
            var form = $(this).closest('form');

            form.validate({
                rules: {
                    @if (Request::segment(4) == 'delete' || (Request::segment(4) != 'add' && is_null($content->variableLang(Request::segment(6)))))                    
                        title: {
                            required: true
                        },
                    @endif
                    @if (Request::segment(4) == 'add')
                        type: {
                            required: true
                        },
                    @endif
                    order: {
                        required: true
                    }
                }
            });

            if (!form.valid()) {
                return;
            }

            @if (Request::segment(4) == 'edit' && !is_null(Request::segment(6)))
                @if ($content->type == 'form')
                    $('#formdata').val(formBuilder.actions.getData('json'));
                @endif
            @endif
            
            btn.addClass('m-loader m-loader--right m-loader--light').attr('disabled', true);
            form.submit();
        });
    });
</script>

<style type="text/css">
    .m-portlet__body > .form-group{
        display: flex;
    }
</style>
@endsection
