@extends('layouts.webshell')

@section('content')
    
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">
                    Menü
                </h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="{{ url('dashboard') }}" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator"> - </li>
                    <li class="m-nav__item">
                        <a href="{{ url('menu/list') }}" class="m-nav__link">
                            <span class="m-nav__link-text">
                                Menü Listesi
                            </span>
                        </a>
                    </li>
                    <li class="m-nav__separator"> - </li>
                    <li class="m-nav__item">
                        <span class="m-nav__link-text">
                            İçerik Listesi ({{ $menu->variableLang($langs->first()->code)->name }})
                        </span>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <div class="m-content">
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            İçerik Listesi ({{ $menu->variableLang($langs->first()->code)->name }})
                        </h3>
                    </div>
                </div>
                <div class="m-portlet__head-tools">
                    <ul class="m-portlet__nav">
                        <li class="m-portlet__nav-item">
                            <a href="{{ url('menu') }}/{{ Request::segment(2) }}/{{ Request::segment(3) }}/add" class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                                <span>
                                    <i class="fa fa-plus"></i>
                                    <span>
                                        Yeni İçerik Ekle
                                    </span>
                                </span>
                            </a>
                            <a href="{{ url('menu') }}/{{ Request::segment(2) }}/{{ Request::segment(3) }}/addexist" class="btn btn-info m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                                <span>
                                    <i class="fa fa-th-large"></i>
                                    <span>
                                        Mevcut İçerik Ekle
                                    </span>
                                </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="m-portlet__body">
                <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                <input type="hidden" name="menu_id" id="menu_id" value="{{ Request::segment(3) }}">
                <table class="table table-striped- table-bordered table-hover table-checkable" id="contentDataTable">
                    <thead>
                        <tr>
                            <th>
                                ID
                            </th>
                            <th width="30">
                                Sıra
                            </th>
                            <th>
                                Adı
                            </th>
                            <th>
                                Tip
                            </th>
                            <th>
                                Eklentiler
                            </th>
                            <th width="50">
                                Durum
                            </th>
                            <th>
                                İşlemler
                            </th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>

@endsection

@section('inline-scripts')
<script type="text/javascript">
    
    $(document).ready(function(){

        var table = $('#contentDataTable').DataTable({
            responsive: true,
            dom: `<'row'<'col-sm-12'tr>> <'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
            lengthMenu: [5, 10, 25, 50, 100],
            pageLength: 50,
            order: [[1, 'asc']],
            language: {
                "lengthMenu": "Her sayfada _MENU_ kayıt göster",
                "info": "Sayfa: _PAGE_/_PAGES_ ",
                "infoEmpty": "Kayıt Yok.",
                "zeroRecords": "Kayıt Yok.",
                "infoFiltered": "(Toplam _MAX_ kayıttan filtrelendi)",
                "processing": "Yükleniyor...",
            },
            searching: true,
            searchDelay: 500,
            processing: true,
            serverSide: true,
            stateSave: true,
            ajax: {
                url: "/getContent", // ajax source
                type: "POST",
                headers: { 'X-CSRF-TOKEN': $('#token').val(), 'MENU-ID': $('#menu_id').val() },
                data: {
                    // parameters for custom backend script demo
                    menuId: $('#menu_id').val(),
                    columnsDef: ['id', 'order', 'title', 'type', 'addons', 'status', 'actions'],
                },
            },
            columns: [
                {name: 'id'},
                {name: 'order'},
                {name: 'title'},
                {name: 'type'},
                {name: 'addons'},
                {name: 'status'},
                {name: 'actions'},
            ],
            columnDefs: [
                {
                    targets: 0,
                    orderable: false,
                    visible: false,
                },
                {
                    targets: 3,
                    render: function(data, type, full, meta) {
                        var ctype = {
                            'text': {'title': 'Metin & HTML', 'class': 'm-badge--accent'},
                            'photo': {'title': 'Fotoğraf', 'class': ' m-badge--brand'},
                            'photogallery': {'title': 'Foto Galeri', 'class': ' m-badge--info'},
                            'link': {'title': 'Button & Link', 'class': ' m-badge--danger'},
                            'form': {'title': 'Form', 'class': ' m-badge--success'}
                        };
                        if (typeof ctype[data] === 'undefined') {
                            return data;
                        }
                        return '<span class="m-badge ' + ctype[data].class + ' m-badge--wide">' + ctype[data].title + '</span>';
                    },
                },
                {

                    targets: -2,
                    render: function(data, type, full, meta) {
                        var status = {
                            'active': {'title': 'Aktif', 'class': 'm-badge--brand'},
                            'passive': {'title': 'Pasif', 'class': ' m-badge--metal'}
                        };
                        if (typeof status[data] === 'undefined') {
                            return data;
                        }
                        return '<span class="m-badge ' + status[data].class + ' m-badge--wide">' + status[data].title + '</span>';
                    },
                },
                {
                    targets: -1,
                    title: 'İşlemler',
                    orderable: false,
                    render: function(data, type, full, meta) {
                        var editButtons = '';
                        @foreach ($langs as $lang)
                            editButtons += `<a href="{{ url('menu/content') }}/{{ $menu->id }}/edit/`+full[0]+`/{{ $lang->code }}" class="m-portlet__nav-link btn btn-sm m-btn btn-outline-brand m-btn--pill" title="Düzenle {{ $lang->code }}">
                                {{ $lang->code }}
                            </a> `;
                        @endforeach

                        if(full[3] == 'photogallery'){
                            editButtons += `<a href="{{ url('menu/content') }}/{{ $menu->id }}/galup/`+full[0]+`" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" title="Fotoğraf Yükle">
                                <i class="fa fa-upload"></i>
                            </a> `;
                        }

                        return editButtons+`
                        <a href="{{ url('menu/content') }}/{{ $menu->id }}/edit/`+full[0]+`" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" title="Ayarlar">
                          <i class="fa fa-cogs"></i>
                        </a>
                        @if ($menu->type == 'list')
                        <a href="{{ url('menu/content') }}/{{ $menu->id }}/addons/`+full[0]+`" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" title="Eklentiler">
                          <i class="fa fa-plus-square"></i>
                        </a>
                        @endif
                        `;
                    },
                }
            ],
        });

    });
</script>
@endsection
