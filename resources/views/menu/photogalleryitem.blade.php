@extends('layouts.webshell')

@section('content')

	<div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">
                    Menü 
                </h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="{{ url('dashboard') }}" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator"> - </li>
                    <li class="m-nav__item">
                        <a href="{{ url('menu/list') }}" class="m-nav__link">
                            <span class="m-nav__link-text">
                                Menü Listesi
                            </span>
                        </a>
                    </li>
                    <li class="m-nav__separator"> - </li>
                    <li class="m-nav__item">
                        <a href="{{ url('menu') }}/{{ Request::segment(2) }}/{{ Request::segment(3) }}" class="m-nav__link">
                            <span class="m-nav__link-text">
                                İçerik Listesi ({{ $menu->variableLang($activeLang->first()->code)->name }})
                            </span>
                        </a>
                    </li>
                    <li class="m-nav__separator"> - </li>
                    <li class="m-nav__item">
                        <a href="{{ url('menu') }}/{{ Request::segment(2) }}/{{ Request::segment(3) }}/{{ Request::segment(4) }}/{{ Request::segment(5) }}" class="m-nav__link">
                            <span class="m-nav__link-text">
                                @if (Request::segment(4) == 'add')
                                    İçerik Ekle
                                @elseif (Request::segment(4) == 'galup')
                                    Fotoğraf Yükle
                                @elseif (Request::segment(4) == 'edit')
                                    @if (is_null(Request::segment(6)))
                                        İçerik Detayları
                                    @else
                                        @if (Request::segment(6) == 'photogallery_edit')
                                            Fotoğraf Düzenle
                                        @else
                                            İçerik Düzenle
                                        @endif
                                    @endif
                                @elseif (Request::segment(4) == 'delete')
                                    İçerik Sil
                                @endif
                            </span>
                        </a>
                    </li>
                    <li class="m-nav__separator"> - </li>
                    <li class="m-nav__item">
                        <span class="m-nav__link-text">
                            @if (Request::segment(6) == 'photogallery_edit')
                                Fotoğraf Düzenle
                            @endif
                        </span>
                    </li>
                </ul>
            </div>
        </div>
    </div>

	<div class="m-content">
		
		<div class="m-portlet m-portlet--mobile">
			
            <div class="m-portlet__head">
				<div class="m-portlet__head-caption">
					<div class="m-portlet__head-title">
						<h3 class="m-portlet__head-text">
							Foto Galeri Düzenle
						</h3>
					</div>
				</div>
				<div class="m-portlet__head-tools">
                    @if (Request::segment(6) == 'photogallery_edit')
                        <ul class="m-portlet__nav">
                            <li class="m-portlet__nav-item">
                                <a href="{{ url('menu/content/'.Request::segment(3).'/edit/'.Request::segment(5).'/photogallery_delete/'.Request::segment(7)) }}" class="btn btn-danger m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill" title="Fotoğraf Sil">
                                    <i class="fa fa-trash"></i>
                                </a>
                            </li>
                        </ul>
                    @endif
				</div>
			</div>

			<form class="m-form m-form--fit m-form--label-align-right" method="POST" action="{{ url('menu/content/'.Request::segment(3).'/edit/'.Request::segment(5).'/photogallery_save') }}" id="photoGalleryForm">
                {{ csrf_field() }}
                <input type="hidden" name="crud" value="{{ Request::segment(6) }}">
                <input type="hidden" name="menu_id" value="{{ Request::segment(3) }}">
                <input type="hidden" name="content_id" value="{{ Request::segment(5) }}">
                <input type="hidden" name="photo_gallery_id" value="{{ Request::segment(7) }}">
                <div class="m-portlet__body">
                    <div class="form-group m-form__group row">
                        <div class="col-10 ml-auto">
                            <h3 class="m-form__section">
                                Fotoğraf Detayları
                            </h3>
                        </div>
                    </div>
                    <div class="offset-2 col-7">
                        <img class="img-fluid" src="{{ url('upload/small/'.$photogalleryitem->url) }}" />
                    </div>
                    <div class="form-group m-form__group row @if ($errors->has('name')) has-danger @endif">
                        <label for="example-text-input" class="col-2 col-form-label">
                            Adı
                        </label>
                        <div class="col-7">
                            <input class="form-control m-input" type="text" id="name" name="name" 
                                @if (Request::segment(6) != 'add' && empty(old('name'))) 
                                    value="{{ $photogalleryitem->name }}"
                                @else 
                                    value="{{ old('name') }}" 
                                @endif

                                @if (Request::segment(6) == 'view' || Request::segment(6) == 'photogallery_delete')
                                    disabled="disabled" 
                                @endif
                            autofocus>
                            @if ($errors->has('name'))
                                <div id="name-error" class="form-control-feedback">{{ $errors->first('name') }}</div>
                            @endif
                        </div>
                    </div>
                    
                    <div class="form-group m-form__group row @if ($errors->has('description')) has-danger @endif">
                        <label for="example-text-input" class="col-2 col-form-label">
                            Açıklama
                        </label>
                        <div class="col-7">
                            <textarea class="form-control m-input" id="description" name="description" rows="3"
                                @if (Request::segment(6) == 'view' || Request::segment(6) == 'photogallery_delete')
                                    disabled="disabled" 
                                @endif
                            >@if (Request::segment(6) != 'add' && empty(old('description'))){{ $photogalleryitem->description }}@else{{ old('description') }}@endif</textarea>

                            @if ($errors->has('description'))
                                <div id="description-error" class="form-control-feedback">{{ $errors->first('description') }}</div>
                            @endif
                        </div>
                    </div>
                    <div class="form-group m-form__group row @if ($errors->has('order')) has-danger @endif">
                        <label for="example-text-input" class="col-2 col-form-label">
                            Sıralama
                        </label>
                        <div class="col-7">
                            <input class="form-control m-input" type="number" min="1" max="100000" id="order" name="order" 
                                @if (Request::segment(6) != 'add' && empty(old('order'))) 
                                    value="{{ $photogalleryitem->order }}"
                                @else 
                                    value="{{ old('order') }}" 
                                @endif

                                @if (Request::segment(6) == 'view' || Request::segment(6) == 'photogallery_delete')
                                    disabled="disabled" 
                                @endif
                            required autofocus>
                            @if ($errors->has('order'))
                                <div id="order-error" class="form-control-feedback">{{ $errors->first('order') }}</div>
                            @endif
                        </div>
                    </div>
                    <div class="m-form__group form-group row">
                        <label for="example-text-input" class="col-2 col-form-label">
                            Pasif / Aktif
                        </label>
                        <div class="col-3">
                            <span class="m-switch">
                                <label>
                                    <input type="checkbox" @if ($photogalleryitem->status == 'active' || Request::segment(6) == 'add') {{ 'checked="checked"' }} @endif  id="status" name="status" value="active" 
                                        @if (Request::segment(6) == 'view' || Request::segment(6) == 'photogallery_delete')
                                            disabled="disabled" 
                                        @endif
                                    />
                                    <span></span>
                                </label>
                            </span>
                        </div>
                    </div>

                </div>

                <div class="m-portlet__foot m-portlet__foot--fit">
                    <div class="m-form__actions">
                        <div class="row">
                            <div class="col-2"></div>
                            <div class="col-7">
                                @if (Request::segment(6) == 'view')
                                    <a href="{{ url('menu/content/'.Request::segment(3).'/edit/'.Request::segment(5)) }}" class="btn btn-secondary m-btn m-btn--air m-btn--custom">
                                        Geri
                                    </a>
                                @elseif (Request::segment(6) == 'add' || Request::segment(6) == 'photogallery_edit')
                                    <button class="btn btn-accent m-btn m-btn--air m-btn--custom" id="postBtn">
                                        Kaydet
                                    </button>
                                    &nbsp;&nbsp;
                                    <a href="{{ url('menu/content/'.Request::segment(3).'/edit/'.Request::segment(5)) }}" class="btn btn-secondary m-btn m-btn--air m-btn--custom">
                                        Vazgeç
                                    </a>
                                @elseif (Request::segment(6) == 'photogallery_delete')
                                    <div class="alert alert-danger" role="alert">
                                        <strong> Siliyorsunuz... </strong>
                                        Bu işlem geri alınamaz ve ilişkili kayıtlarda soruna sebep olabilir.
                                    </div>
                                    <button class="btn btn-danger m-btn m-btn--air m-btn--custom" id="postBtn">
                                        Kaydı Sil
                                    </button>
                                    &nbsp;&nbsp;
                                    <a href="{{ url('menu/content/'.Request::segment(3).'/edit/'.Request::segment(5)) }}" class="btn btn-secondary m-btn m-btn--air m-btn--custom">
                                        Vazgeç
                                    </a>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </form>

		</div>
	</div>
@endsection


@section('inline-scripts')
<script type="text/javascript">
    $(document).ready(function(){

        $('#postBtn').click(function(e) {

            e.preventDefault();
            var btn = $(this);
            var form = $(this).closest('form');

            form.validate({
                rules: {
                    order: {
                        required: true
                    }
                }
            });

            if (!form.valid()) {
                return;
            }
            
            btn.addClass('m-loader m-loader--right m-loader--light').attr('disabled', true);
            form.submit();
        });
    });
</script>
@endsection
