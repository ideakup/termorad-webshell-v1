@extends('layouts.webshell')

@section('content')

	<div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">
                    Menü 
                </h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="{{ url('dashboard') }}" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator"> - </li>
                    <li class="m-nav__item">
                        <a href="{{ url('menu/list') }}" class="m-nav__link">
                            <span class="m-nav__link-text">
                                Menü Listesi
                            </span>
                        </a>
                    </li>
                    <li class="m-nav__separator"> - </li>
                    <li class="m-nav__item">
                        <a href="{{ url('menu') }}/{{ Request::segment(2) }}/{{ Request::segment(3) }}" class="m-nav__link">
                            <span class="m-nav__link-text">
                                Slide Listesi ({{ $menu->variableLang($langs->first()->code)->name }})
                            </span>
                        </a>
                    </li>
                    <li class="m-nav__separator"> - </li>
                    <li class="m-nav__item">
                        <span class="m-nav__link-text">
                            @if (Request::segment(4) == 'add')
                                Slide Ekle
                            @elseif (Request::segment(4) == 'edit')
                                @if (is_null(Request::segment(6)))
                                    Slide Detayları
                                @else
                                    Slide Düzenle
                                @endif
                            @elseif (Request::segment(4) == 'delete')
                                Slide Sil
                            @endif
                        </span>
                    </li>
                </ul>
            </div>
        </div>
    </div>

	<div class="m-content">
		
		<div class="m-portlet m-portlet--mobile">
			
            <div class="m-portlet__head">
				<div class="m-portlet__head-caption">
					<div class="m-portlet__head-title">
						<h3 class="m-portlet__head-text">
							@if (Request::segment(4) == 'add')
                                Slide Ekle
                            @elseif (Request::segment(4) == 'edit')
                                @if (is_null(Request::segment(6)))
                                    Slide Detayları
                                @else
                                    Slide Düzenle
                                @endif
                            @elseif (Request::segment(4) == 'delete')
                                Slide Sil ({{ $menu->variableLang($langs->first()->code)->name }})
                            @endif
						</h3>
					</div>
				</div>

				<div class="m-portlet__head-tools">
                    @if (Request::segment(4) == 'edit' && is_null(Request::segment(6)))
                        <a href="{{ url('menu/stslider') }}/{{ Request::segment(3) }}/delete/{{Request::segment(5)}}" class="btn btn-danger m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill" title="İçerik Sil">
                            <i class="fa fa-trash"></i>
                        </a>
                    @endif
				</div>
			</div>

            <form class="m-form m-form--fit m-form--label-align-right" method="POST" action="{{ url('menu/stslider/save') }}" id="stsliderForm">
                {{ csrf_field() }}
                <input type="hidden" name="crud" value="{{ Request::segment(4) }}">
                <input type="hidden" name="menu_id" id="menu_id" value="{{ Request::segment(3) }}">
                <input type="hidden" name="slider_id" id="slider_id" value="{{ Request::segment(5) }}">
                <input type="hidden" name="lang" value="{{ Request::segment(6) }}">
                <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">

                <div class="m-portlet__body">

                    <div class="form-group m-form__group row">
                        <div class="col-10 ml-auto">
                            <h3 class="m-form__section">
                                Slide Bilgileri
                            </h3>
                        </div>
                    </div>

                    <div class="form-group m-form__group row @if ($errors->has('title')) has-danger @endif">
                        <label for="example-text-input" class="col-2 col-form-label">
                            Başlık
                        </label>
                        <div class="col-7">
                            <input class="form-control m-input" type="text" id="title" name="title" 
                                @if (Request::segment(4) != 'add' && empty(old('title'))) 
                                    value="@if(is_null($slide->variableLang(Request::segment(6)))){{ $slide->variableLang($langs->first()->code)->title }}@else{{ $slide->variableLang(Request::segment(6))->title }}@endif"
                                @else 
                                    value="{{ old('title') }}" 
                                @endif
                                
                                @if (Request::segment(4) == 'delete' || (Request::segment(4) != 'add' && is_null($slide->variableLang(Request::segment(6)))))
                                    disabled="disabled" 
                                @endif
                            required autofocus>
                            @if ($errors->has('title'))
                                <div id="title-error" class="form-control-feedback">{{ $errors->first('title') }}</div>
                            @endif
                        </div>
                    </div>

                    <div class="form-group m-form__group row @if ($errors->has('description')) has-danger @endif">
                        <label for="example-text-input" class="col-2 col-form-label">
                            Açıklama
                        </label>
                        <div class="col-7">
                            <textarea class="form-control m-input" id="description" name="description" rows="3"
                                @if (Request::segment(4) == 'delete' || (Request::segment(4) != 'add' && is_null($slide->variableLang(Request::segment(6)))))
                                    disabled="disabled" 
                                @endif
                            >@if(Request::segment(4) != 'add' && empty(old('description')))@if(is_null($slide->variableLang(Request::segment(6)))){{ $slide->variableLang($langs->first()->code)->description }}@else{{ $slide->variableLang(Request::segment(6))->description }}@endif @else{{ old('description') }}@endif</textarea>

                            @if ($errors->has('description'))
                                <div id="description-error" class="form-control-feedback">{{ $errors->first('description') }}</div>
                            @endif
                        </div>
                    </div>

                    <div class="form-group m-form__group row @if ($errors->has('button_text')) has-danger @endif">
                        <label for="example-text-input" class="col-2 col-form-label">
                            Button Metni
                        </label>
                        <div class="col-7">
                            <input class="form-control m-input" type="text" id="button_text" name="button_text" 
                                @if (Request::segment(4) != 'add' && empty(old('button_text'))) 
                                    value="@if(is_null($slide->variableLang(Request::segment(6)))){{ $slide->variableLang($langs->first()->code)->button_text }}@else{{ $slide->variableLang(Request::segment(6))->button_text }}@endif"
                                @else 
                                    value="{{ old('button_text') }}" 
                                @endif
                                
                                @if (Request::segment(4) == 'delete' || (Request::segment(4) != 'add' && is_null($slide->variableLang(Request::segment(6)))))
                                    disabled="disabled" 
                                @endif
                            >
                            @if ($errors->has('button_text'))
                                <div id="button_text-error" class="form-control-feedback">{{ $errors->first('button_text') }}</div>
                            @endif
                        </div>
                    </div>

                    <div class="form-group m-form__group row @if ($errors->has('button_url')) has-danger @endif">
                        <label for="example-text-input" class="col-2 col-form-label">
                            Button URL
                        </label>
                        <div class="col-7">
                            <input class="form-control m-input" type="text" id="button_url" name="button_url" 
                                @if (Request::segment(4) != 'add' && empty(old('button_url'))) 
                                    value="@if(is_null($slide->variableLang(Request::segment(6)))){{ $slide->variableLang($langs->first()->code)->button_url }}@else{{ $slide->variableLang(Request::segment(6))->button_url }}@endif"
                                @else 
                                    value="{{ old('button_url') }}" 
                                @endif
                                
                                @if (Request::segment(4) == 'delete' || (Request::segment(4) != 'add' && is_null($slide->variableLang(Request::segment(6)))))
                                    disabled="disabled" 
                                @endif
                            >
                            @if ($errors->has('button_url'))
                                <div id="button_url-error" class="form-control-feedback">{{ $errors->first('button_url') }}</div>
                            @endif
                        </div>
                    </div>

                    @if (Request::segment(4) == 'add' || (Request::segment(4) == 'edit' && is_null(Request::segment(6))))
                        
                        <div class="form-group m-form__group row @if ($errors->has('order')) has-danger @endif">
                            <label for="example-text-input" class="col-2 col-form-label">
                                Sıralama
                            </label>
                            <div class="col-7">
                                <input class="form-control m-input" type="number" min="1" max="100000" id="order" name="order" 
                                    @if (Request::segment(4) != 'add' && empty(old('order'))) 
                                        value="{{ $slide->order }}"
                                    @else 
                                        value="{{ old('order') }}" 
                                    @endif

                                    @if (Request::segment(4) == 'delete')
                                        disabled="disabled" 
                                    @endif
                                required autofocus>
                                @if ($errors->has('order'))
                                    <div id="order-error" class="form-control-feedback">{{ $errors->first('order') }}</div>
                                @endif
                            </div>
                        </div>

                        <div class="m-form__group form-group row">
                            <label for="example-text-input" class="col-2 col-form-label">
                                Pasif / Aktif
                            </label>
                            <div class="col-3">
                                <span class="m-switch">
                                    <label>
                                        <input type="checkbox" @if (Request::segment(4) == 'add' || $slide->status == 'active') {{ 'checked="checked"' }} @endif id="status" name="status" value="active" 
                                            @if (Request::segment(4) == 'delete')
                                                disabled="disabled" 
                                            @endif
                                        />
                                        <span></span>
                                    </label>
                                </span>
                            </div>
                        </div>
                    @endif

                    @if (Request::segment(4) != 'add' && (Request::segment(4) == 'edit' && !is_null(Request::segment(6))))
                        <hr>
                        <div class="form-group m-form__group row">
                            <div class="col-10 ml-auto">
                                <h3 class="m-form__section">
                                    Slide Görseli
                                </h3>
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <div class="col-12" id="imgContainer">
                                @if (is_null($slide->variableLang(Request::segment(6))->image_url))
                                    <div class="col-10 ml-auto">
                                        <div class="m-alert m-alert--outline alert alert-warning alert-dismissible fade show" role="alert">
                                            <strong>Uyarı!</strong> Yüklenmiş Görsel Bulunmuyor...
                                        </div>
                                    </div>
                                @else
                                    <img class="img-fluid" src="{{ url('upload/xlarge/'.$slide->variableLang(Request::segment(6))->image_url) }}" />
                                @endif
                            </div>
                        </div>
                        @if (Request::segment(4) == 'edit')
                            <div class="form-group m-form__group row">
                                <div class="col-12">
                                    <div class="m-dropzone dropzone" action="{{ url('/uploadFile') }}" id="dropzone-stslider-upload">
                                        <div class="m-dropzone__msg dz-message needsclick">
                                            <h3 class="m-dropzone__msg-title">
                                                Dosya yüklemek için dosyayı buraya sürükleyin yada bu alan tıklayın.
                                            </h3>
                                            <span class="m-dropzone__msg-desc">
                                                Bu alan <strong>jpg</strong> ve <strong>png</strong> formatlarını yüklemenize izin verir.
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                    @endif
                </div>

                <div class="m-portlet__foot m-portlet__foot--fit">
                    <div class="m-form__actions">
                        <div class="row">
                            <div class="col-2"></div>
                            <div class="col-7">
                                @if (Request::segment(4) == 'view')
                                    <a href="{{ url('menu/stslider') }}/{{ Request::segment(3) }}" class="btn btn-secondary m-btn m-btn--air m-btn--custom">
                                        Geri
                                    </a>
                                @elseif (Request::segment(4) == 'add' || Request::segment(4) == 'edit')
                                    <button class="btn btn-accent m-btn m-btn--air m-btn--custom" id="postBtn">
                                        Kaydet
                                    </button>
                                    &nbsp;&nbsp;
                                    <a href="{{ url('menu/stslider') }}/{{ Request::segment(3) }}" class="btn btn-secondary m-btn m-btn--air m-btn--custom">
                                        Vazgeç
                                    </a>
                                @elseif (Request::segment(4) == 'delete')
                                    <div class="alert alert-danger" role="alert">
                                        <strong> Siliyorsunuz... </strong>
                                        Bu işlem geri alınamaz ve ilişkili kayıtlarda soruna sebep olabilir.
                                    </div>
                                    <button class="btn btn-danger m-btn m-btn--air m-btn--custom" id="postBtn">
                                        Kaydı Sil
                                    </button>
                                    &nbsp;&nbsp;
                                    <a href="{{ url('menu/stslider') }}/{{ Request::segment(3) }}" class="btn btn-secondary m-btn m-btn--air m-btn--custom">
                                        Vazgeç
                                    </a>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>

            </form>

		</div>
	</div>
@endsection


@section('inline-scripts')
<script type="text/javascript">

    Dropzone.options.dropzoneStsliderUpload = {
        headers: { 'X-CSRF-TOKEN': $('#token').val() },
        paramName: "file", // The name that will be used to transfer the file
        maxFiles: 1,
        maxFilesize: 5, // MB
        addRemoveLinks: true,
        accept: function(file, done) {
            done();
        },
        init: function () {
            //console.log('dropzone init');
        },
        sending: function(file, xhr, formData){
            formData.append('uploadType', 'stslider');
            formData.append('slider_id', $('#slider_id').val());
            formData.append('lang_code', '{{ Request::segment(6) }}');
        },
        success: function(file, xhr, event){
            var data = jQuery.parseJSON(xhr);
            $("#imgContainer").html('<img class="img-fluid" src="{{ url('') }}/upload/xlarge/' + data.location + '" />');
            this.removeFile(file);
        }
    };

</script>
@endsection
