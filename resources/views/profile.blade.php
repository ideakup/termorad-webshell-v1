@extends('layouts.webshell')

@section('content')

	<!-- BEGIN: Subheader -->
	<div class="m-subheader ">
	    <div class="d-flex align-items-center">
	        <div class="mr-auto">
	            <h3 class="m-subheader__title ">
	                Profil ve Ayarlar
	            </h3>
	        </div>
	    </div>
	</div>
	<!-- END: Subheader -->
	<div class="m-content">
	    <div class="row">
	        <div class="col-xl-3 col-lg-4">
	            <div class="m-portlet m-portlet--full-height">
	                <div class="m-portlet__body">
	                    <div class="m-card-profile">
	                        <div class="m-card-profile__title m--hide">
	                            Profil
	                        </div>
	                        <div class="m-card-profile__pic">
	                            <div class="m-card-profile__pic-wrapper">
	                                <img src="{{ (Auth::user()->avatar == null) ? asset('images/avatars/default.jpg') : asset('img/avatars/'.Auth::user()->avatar) }}" alt="" />
	                            </div>
	                        </div>
	                        <div class="m-card-profile__details">
	                            <span class="m-card-profile__name">
	                                {{ Auth::user()->name }}
	                            </span>
	                            <span class="m-card-user__email m--font-weight-300">
                                    Geliştirici
                                </span>
	                        </div>
	                    </div>
	                </div>
	            </div>
	        </div>
	        <div class="col-xl-9 col-lg-8">
	            <div class="m-portlet m-portlet--full-height m-portlet--tabs  ">
	                <div class="m-portlet__head">
	                    <div class="m-portlet__head-tools">
	                        <ul class="nav nav-tabs m-tabs m-tabs-line m-tabs-line--left m-tabs-line--primary" role="tablist">
	                            <li class="nav-item m-tabs__item ">
	                                <a class="nav-link m-tabs__link @if (session('tabname') == null || session('tabname') == 'personaldetails' ) {{ 'active' }} @endif" data-toggle="tab" href="#m_personal_details_tab" role="tab">
	                                    <i class="flaticon-share m--hide"></i>
	                                    Profil
	                                </a>
	                            </li>
	                            <li class="nav-item m-tabs__item ">
	                                <a class="nav-link m-tabs__link @if (session('tabname') == 'changepassword' ) {{ 'active' }} @endif" data-toggle="tab" href="#m_change_password_tab" role="tab" aria-selected="true">
	                                    Şifre Değiştir
	                                </a>
	                            </li>
	                        </ul>
	                    </div>
	                    <div class="m-portlet__head-tools">
	                    </div>
	                </div>
	                <div class="tab-content">

	                    <div class="tab-pane @if (session('tabname') == null || session('tabname') == 'updateprofile' ) {{ 'active' }} @endif" id="m_personal_details_tab">
	                    	<form class="m-form m-form--fit m-form--label-align-right">
	                            <div class="m-portlet__body">
	                                <div class="form-group m-form__group row">
	                                    <div class="col-10 ml-auto">
	                                        <h3 class="m-form__section">
	                                            Profil
	                                        </h3>
	                                    </div>
	                                </div>
	                                <div class="form-group m-form__group row">
	                                    <label for="example-text-input" class="col-3 col-form-label">
	                                        Tam Adı
	                                    </label>
	                                    <div class="col-7">
	                                        <input class="form-control m-input" type="text" value="{{ Auth::user()->name }}" disabled="disabled">
	                                    </div>
	                                </div>
	                                
	                            </div>
                            </form>
	                    </div>

	                    <div class="tab-pane @if (session('tabname') == 'changepassword' ) {{ 'active' }} @endif" id="m_change_password_tab">
	                    	<form class="m-form m-form--fit m-form--label-align-right" method="POST" action="{{ url('changePassword') }}" id="changePasswordForm">

				                {{ csrf_field() }}

	                            <div class="m-portlet__body">
	                                <div class="form-group m-form__group row">
	                                    <div class="col-10 ml-auto">
	                                        <h3 class="m-form__section">
	                                            Şifre Değiştir
	                                        </h3>
	                                    </div>
	                                </div>
	                                <div class="form-group m-form__group row @if ($errors->has('oldPassword')) has-danger @endif">
	                                    <label for="example-text-input" class="col-3 col-form-label">
	                                        Old Password
	                                    </label>
	                                    <div class="col-7">
	                                        <input class="form-control m-input" type="password" name="oldPassword" id="oldPassword">
	                                        @if ($errors->has('oldPassword'))
				                                <div id="name-error" class="form-control-feedback">{{ $errors->first('oldPassword') }}</div>
				                            @endif
	                                    </div>
	                                </div>
	                                <div class="form-group m-form__group row @if ($errors->has('newPassword')) has-danger @endif">
	                                    <label for="example-text-input" class="col-3 col-form-label">
	                                        New Password
	                                    </label>
	                                    <div class="col-7">
	                                        <input class="form-control m-input" type="password" name="newPassword" id="newPassword">
	                                        @if ($errors->has('newPassword'))
				                                <div id="name-error" class="form-control-feedback">{{ $errors->first('newPassword') }}</div>
				                            @endif
	                                    </div>
	                                </div>
	                                <div class="form-group m-form__group row @if ($errors->has('newPassword_confirmation')) has-danger @endif">
	                                    <label for="example-text-input" class="col-3 col-form-label">
	                                        Confirm Password
	                                    </label>
	                                    <div class="col-7">
	                                        <input class="form-control m-input" type="password" name="newPassword_confirmation" id="newPassword_confirmation">
	                                        @if ($errors->has('newPassword_confirmation'))
				                                <div id="name-error" class="form-control-feedback">{{ $errors->first('newPassword_confirmation') }}</div>
				                            @endif
	                                    </div>
	                                </div>
	                            </div>
	                            <div class="m-portlet__foot m-portlet__foot--fit">
	                                <div class="m-form__actions">
	                                    <div class="row">
	                                        <div class="col-3"></div>
	                                        <div class="col-7">
	                                            <button class="btn btn-accent m-btn m-btn--air m-btn--custom" id="postBtn">
	                                                Şifre Değiştir
	                                            </button>
	                                        </div>
	                                    </div>
	                                </div>
	                            </div>
	                        </form>
	                    </div>
	                    <!--
	                    	<div class="tab-pane " id="m_user_profile_tab_3"></div>
						-->
	                </div>
	            </div>
	        </div>
	    </div>
	</div>

@endsection


@section('inline-scripts')
<script type="text/javascript">
    $(document).ready(function(){

        $('#postBtn').click(function(e) {
        	console.log();
            e.preventDefault();
            var btn = $(this);
            var form = $(this).closest('form');
            
            form.validate({
                rules: {
                    oldPassword: {
                        required: true
                    },
                    newPassword: {
                        required: true
                    },
                    newPassword_confirmation: {
                        equalTo: "#newPassword"
                    }
                }
            });
			
            if (!form.valid()) {
                return;
            }
            
            btn.addClass('m-loader m-loader--right m-loader--light').attr('disabled', true);
            form.submit();
        
        });

    });
</script>
@endsection

