@extends('layouts.webshellresetpass')

@section('content')

<!-- begin:: Page -->
<div class="m-grid m-grid--hor m-grid--root m-page">
    <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-grid--tablet-and-mobile m-grid--hor-tablet-and-mobile m-login m-login--1 m-login--signin" id="m_login">

        <div class="m-grid__item m-grid__item--order-tablet-and-mobile-2 m-login__aside">
            <div class="m-stack m-stack--hor m-stack--desktop">
                <div class="m-stack__item m-stack__item--fluid">
                    <div class="m-login__wrapper">

                        <div class="m-login__logo">
                            <a href="#">
                                <img src="{{ asset('images/ws-logo.png') }}">
                            </a>
                        </div>

                        <div class="m-login__signin">
                            <div class="m-login__head">
                                <h3 class="m-login__title">
                                    Şifreni Sıfırla
                                </h3>
                            </div>

                            <form class="m-login__form m-form" method="POST" action="{{ route('password.request') }}" id="resetPasswordForm">
                                {{ csrf_field() }}
                                <input type="hidden" name="token" value="{{ $token }}">

                                @if ($errors->has('email'))
                                    <div class="m-alert m-alert--outline alert alert-danger alert-dismissible" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Kapat"></button>
                                        <span>{{ $errors->first('email') }}</span>
                                    </div>
                                @endif
                                @if ($errors->has('password'))
                                    <div class="m-alert m-alert--outline alert alert-danger alert-dismissible" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Kapat"></button>
                                        <span>{{ $errors->first('password') }}</span>
                                    </div>
                                @endif

                                <div class="form-group m-form__group">
                                    <input class="form-control m-input" type="email" id="email" name="email" placeholder="E-posta" value="{{ old('email') }}" required autofocus>    
                                </div>

                                <div class="form-group m-form__group">
                                    <input class="form-control m-input m-login__form-input--last" type="password" id="password" name="password" placeholder="Şifre" required>
                                </div>

                                <div class="form-group m-form__group">
                                    <input class="form-control m-input m-login__form-input--last" type="password" id="password_confirmation" name="password_confirmation" placeholder="Şifre Tekrar" required>
                                </div>

                                <div class="m-login__form-action">
                                    <button id="m_reset_password_submit" class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air">
                                        Şifreni Sıfırla
                                    </button>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        @include('partials.auth.info')
        
    </div>
</div>
<!-- end:: Page -->
@endsection