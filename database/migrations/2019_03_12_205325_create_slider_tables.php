<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSliderTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('slider', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('menu_id')->unsigned();
            
            $table->integer('order');
            $table->enum('status', ['active', 'passive'])->default('active');
            $table->enum('deleted', ['no', 'yes'])->default('no');
            $table->timestamps();
        });

        Schema::table('slider', function (Blueprint $table) {
            $table->foreign('menu_id')->references('id')->on('menu');
        });

        Schema::create('slidervariable', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('slider_id')->unsigned();
            $table->string('lang_code',6);

            $table->string('title', 190);
            $table->text('description')->nullable();
            $table->string('button_text', 190)->nullable();
            $table->string('button_url', 190)->nullable();
            $table->string('image_url', 190)->nullable();
            $table->timestamps();
        });

        Schema::table('slidervariable', function (Blueprint $table) {
            $table->foreign('slider_id')->references('id')->on('slider');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('slidervariable');
        Schema::dropIfExists('slider');
    }
}
