<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenuTables extends Migration
{
    public function up()
    {
        Schema::create('menu', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('top_id')->unsigned()->nullable();
            $table->enum('type', ['content', 'list', 'link', 'photogallery'])->default('content');
            $table->mediumText('description')->nullable();
            
            $table->enum('position', ['top', 'aside', 'all', 'none'])->default('top');
            $table->enum('headertheme', ['general', 'home'])->default('general');
            $table->enum('slidertype', ['image', 'slider', 'no'])->default('no');
            $table->enum('breadcrumbvisible', ['yes', 'no'])->default('no');
            $table->enum('asidevisible', ['yes', 'no'])->default('no');
            $table->enum('listtype', ['normal', 'headed', 'pyramid'])->default('normal')->nullable();

            $table->integer('order');
            $table->enum('status', ['active', 'passive'])->default('active');
            $table->enum('deleted', ['no', 'yes'])->default('no');
            $table->timestamps();
        });

        Schema::table('menu', function (Blueprint $table) {
            $table->foreign('top_id')->references('id')->on('menu');
        });

        Schema::create('menuvariable', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('menu_id')->unsigned();
            $table->string('lang_code',6);
            $table->string('name',190);
            $table->string('slug',190)->nullable();
            $table->string('title',190);
            $table->mediumText('stvalue')->nullable();
            $table->timestamps();
        });

        Schema::table('menuvariable', function (Blueprint $table) {
            $table->foreign('menu_id')->references('id')->on('menu');
        });
    }

    public function down()
    {   
        Schema::dropIfExists('menuvariable');
        Schema::dropIfExists('menu');
    }
}
