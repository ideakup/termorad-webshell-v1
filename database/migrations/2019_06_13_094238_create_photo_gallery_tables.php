<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhotoGalleryTables extends Migration
{

    public function up()
    {
        Schema::create('photogallery', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('content_id')->unsigned();
            
            $table->string('name', 190)->nullable();
            $table->string('description', 190)->nullable();
            $table->string('url', 190)->nullable();
            $table->integer('order');
            $table->enum('status', ['active', 'passive'])->default('active');
            $table->enum('deleted', ['no', 'yes'])->default('no');
            $table->timestamps();
        });

        Schema::table('photogallery', function (Blueprint $table) {
            $table->foreign('content_id')->references('id')->on('content');
        });
    }

    public function down()
    {
        Schema::dropIfExists('photogallery');
    }
}
