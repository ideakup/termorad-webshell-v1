<?php

use Illuminate\Database\Seeder;

class LanguageTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('language')->delete();

        DB::table('language')->insert([
			'name' => 'Türkçe',
			'code' => 'tr',
			'flag_url' => null,
			'order' => '1',
			'status' => 'active',
			'deleted' => 'no'
        ]);

        DB::table('language')->insert([
			'name' => 'English',
			'code' => 'en',
			'flag_url' => null,
			'order' => '2',
			'status' => 'active',
			'deleted' => 'no'
        ]);

    }
}