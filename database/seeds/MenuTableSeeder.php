<?php

use Illuminate\Database\Seeder;

class MenuTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('menu')->delete();
        DB::table('menuvariable')->delete();

        DB::table('menu')->insert([
			'id' => 1,
			'type' => 'content',
			'order' => '1000'
        ]);

        DB::table('menuvariable')->insert([
			'menu_id' => 1,
			'lang_code' => 'tr',
			'name' => 'ANA SAYFA',
			'slug' => 'ana-sayfa',
			'title' => 'ANA SAYFA'
        ]);

        DB::table('menu')->insert([
			'id' => 2,
			'type' => 'content',
			'order' => '2000'
        ]);

        DB::table('menuvariable')->insert([
			'menu_id' => 2,
			'lang_code' => 'tr',
			'name' => 'HAKKIMIZDA',
			'slug' => 'hakkimizda',
			'title' => 'HAKKIMIZDA'
        ]);

        DB::table('menu')->insert([
			'id' => 3,
			'type' => 'list',
			'order' => '3000'
        ]);

        DB::table('menuvariable')->insert([
			'menu_id' => 3,
			'lang_code' => 'tr',
			'name' => 'ÜRÜNLER',
			'slug' => 'urunler',
			'title' => 'ÜRÜNLER'
        ]);

        DB::table('menu')->insert([
			'id' => 4,
			'type' => 'photogallery',
			'order' => '4000'
        ]);

        DB::table('menuvariable')->insert([
			'menu_id' => 4,
			'lang_code' => 'tr',
			'name' => 'GALERİ',
			'slug' => 'galeri',
			'title' => 'GALERİ'
        ]);

        DB::table('menu')->insert([
			'id' => 5,
			'type' => 'list',
			'order' => '5000'
        ]);

        DB::table('menuvariable')->insert([
			'menu_id' => 5,
			'lang_code' => 'tr',
			'name' => 'BLOG',
			'slug' => 'blog',
			'title' => 'BLOG'
        ]);

        DB::table('menu')->insert([
			'id' => 6,
			'type' => 'content',
			'order' => '6000'
        ]);

        DB::table('menuvariable')->insert([
			'menu_id' => 6,
			'lang_code' => 'tr',
			'name' => 'İLETİŞİM',
			'slug' => 'iletisim',
			'title' => 'İLETİŞİM'
        ]);

    }
}
