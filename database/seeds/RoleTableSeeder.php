<?php

use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('roles')->delete();

        DB::table('roles')->insert([
            'name' => 'dev',
            'guard_name' => 'web',
            'description' => 'Sistem geliştiricisidir. Tüm yetkilere sahiptir. En üsttedir.',
        ]);

        DB::table('roles')->insert([
            'name' => 'admin',
            'guard_name' => 'web',
            'description' => 'Sistem adminidir. Geliştiriciyi göremez, bunun dışında tüm yetkiye sahiptir.',
        ]);

        DB::table('roles')->insert([
            'name' => 'user',
            'guard_name' => 'web',
            'description' => 'Sistem kullanıcısıdır. Süreçlere dahil olabilir.',
        ]);
    }
}
