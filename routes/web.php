<?php

Route::redirect('/', '/home', 301);
Route::get('getusername/{email}' , 'ToolsController@getusername');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');

Auth::routes();

Route::get('/', 'AdminController@index');
Route::get('/dashboard', 'AdminController@index');

Route::get('/test', 'MenuController@test');

// AJAX ROUTES
Route::post('/getMenu', 'AdminController@getMenuAjax');
Route::post('/getContent', 'AdminController@getContentAjax');
Route::post('/getTag', 'AdminController@getTagAjax');
Route::post('/getCategory', 'AdminController@getCategoryAjax');
Route::post('/getContentAddons', 'AdminController@getContentAddonsAjax');
Route::post('/getSlider', 'AdminController@getSliderAjax');
Route::post('/getPhotoGallery', 'AdminController@getPhotoGalleryAjax');
Route::post('/getCalendar', 'AdminController@getCalendarAjax');
Route::post('/getForm', 'AdminController@getFormAjax');
Route::post('/uploadFile', 'AdminController@uploadFile');
// AJAX ROUTES

Route::get('/profile', 'ProfileController@index');
Route::post('/changePassword', 'ProfileController@change_password');

/**/Route::get('/sitesettings', 'SiteSettingsController@index');
/**/Route::post('/sitesettings_save', 'SiteSettingsController@save');

/**/Route::get('/language', 'LanguageController@index');
/**/Route::post('/language_save', 'LanguageController@language_save');

/**/Route::get('/photogallery', 'PhotoGalleryController@list');

Route::group(['prefix' => '/menu'], function () {

	Route::get('/list', 'MenuController@list');
	Route::get('/add', 'MenuController@add_edit');
	Route::get('/edit/{id}', 'MenuController@add_edit');
	Route::get('/edit/{id}/{lang}', 'MenuController@add_edit');
	Route::get('/delete/{id}', 'MenuController@delete');
	Route::post('/save', 'MenuController@save');

	Route::group(['prefix' => '/content'], function () {
		
		Route::get('/{id}', 'MenuController@content');
		Route::get('/{id}/add', 'MenuController@content_add_edit');
		Route::get('/{id}/edit/{cid}', 'MenuController@content_add_edit');
		Route::get('/{id}/edit/{cid}/{lang}', 'MenuController@content_add_edit');
		Route::get('/{id}/galup/{cid}', 'MenuController@content_add_edit');
		Route::get('/{id}/delete/{cid}', 'MenuController@content_delete');
		Route::post('/save', 'MenuController@content_save');

		Route::get('/{id}/addexist', 'MenuController@content_addexist');
		Route::post('/saveexist', 'MenuController@content_save_exist');

		Route::group(['prefix' => '/{id}/edit/{cid}'], function () {
			Route::get('/photogallery_edit/{fid}', 'MenuController@photogalleryitem');
			Route::get('/photogallery_delete/{fid}', 'MenuController@photogalleryitem_delete');
			Route::post('/photogallery_save', 'MenuController@photogalleryitem_save');
		});

		Route::get('/{id}/addons/{cid}', 'MenuController@addons');
		Route::get('/{id}/addons/{cid}/add', 'MenuController@addon_add_edit');
		Route::get('/{id}/addons/{cid}/edit/{aid}', 'MenuController@addon_add_edit');
		Route::get('/{id}/addons/{cid}/edit/{aid}/{lang}', 'MenuController@addon_add_edit');
		Route::get('/{id}/addons/{cid}/galup/{aid}', 'MenuController@addon_add_edit');
		Route::get('/{id}/addons/{cid}/delete/{aid}', 'MenuController@addon_delete');
		Route::post('/{id}/addons/{cid}/save', 'MenuController@addon_save');

		Route::group(['prefix' => '/{id}/addons/{cid}/edit/{aid}'], function () {
			Route::get('/photogallery_edit/{fid}', 'MenuController@addon_photogalleryitem');
			Route::get('/photogallery_delete/{fid}', 'MenuController@addon_photogalleryitem_delete');
			Route::post('/photogallery_save', 'MenuController@addon_photogalleryitem_save');
		});

	});

	Route::group(['prefix' => '/stslider'], function () {
		Route::get('/{id}', 'MenuController@stslider');
		Route::get('/{id}/add', 'MenuController@stslider_add_edit');
		Route::get('/{id}/edit/{sid}', 'MenuController@stslider_add_edit');
		Route::get('/{id}/edit/{sid}/{lang}', 'MenuController@stslider_add_edit');
		Route::get('/{id}/delete/{sid}', 'MenuController@stslider_delete');
		Route::post('/save', 'MenuController@stslider_save');
	});

	Route::get('/stimage/{id}', 'MenuController@stimage');
});

Route::group(['prefix' => '/tag'], function () {
	Route::get('/', 'MenuController@tag');
	Route::get('/add', 'MenuController@tag_add_edit');
	Route::get('/edit/{tid}', 'MenuController@tag_add_edit');
	Route::get('/edit/{tid}/{lang}', 'MenuController@tag_add_edit');
	Route::get('/delete/{tid}', 'MenuController@tag_delete');
	Route::post('/save', 'MenuController@tag_save');
});

Route::group(['prefix' => '/category'], function () {
	Route::get('/', 'MenuController@category');
	Route::get('/add', 'MenuController@category_add_edit');
	Route::get('/edit/{cid}', 'MenuController@category_add_edit');
	Route::get('/edit/{cid}/{lang}', 'MenuController@category_add_edit');
	Route::get('/delete/{cid}', 'MenuController@category_delete');
	Route::post('/save', 'MenuController@category_save');
});

Route::group(['prefix' => '/calendar'], function () {
	
	Route::get('/', 'CalendarController@index');
	Route::get('/list', 'CalendarController@list');
	Route::get('/add', 'CalendarController@add_edit');
	Route::get('/edit/{id}', 'CalendarController@add_edit');
	Route::get('/edit/{id}/{lang}', 'CalendarController@add_edit');
	Route::get('/delete/{id}', 'CalendarController@delete');
	Route::post('/save', 'CalendarController@save');
	
	Route::get('/reservations', 'CalendarController@reservations');

});

Route::group(['prefix' => '/form'], function () {

	Route::get('/list', 'MenuController@form_list');
	Route::get('/detail', 'MenuController@form_detail');

});

