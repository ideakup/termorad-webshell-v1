<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TagVariable extends Model
{
    protected $table = 'tagvariable';
}
