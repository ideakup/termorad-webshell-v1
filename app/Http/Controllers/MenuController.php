<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Validator;

use App\Language;
use App\User;
use App\Menu;
use App\MenuVariable;
use App\Content;
use App\ContentVariable;
use App\Tag;
use App\TagVariable;
use App\Category;
use App\CategoryVariable;
use App\FormData;
use App\Form;

use App\MenuHasContent;
use App\ContentHasTag;
use App\ContentHasCategory;

use App\PhotoGallery;
use App\Slider;
use App\SliderVariable;

use App;

class MenuController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function test()
    {
        dump(FormData::select('source_id', 'source_type', 'form_id')->distinct()->get());

        $uniqForms = FormData::select('source_id', 'source_type', 'form_id')->distinct()->get();

        echo 'test';
    }

    /** MENÜ **/
        public function list()
        {
            $langs = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
            return view('menu.list', array('langs' => $langs));
        }

        public function add_edit($id = null, $lang = null)
        {
            $langs = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
            $topmenus = Menu::where('deleted', 'no')->get();
            if(is_null($id)){
                $menu = new Menu();
            }else{
                $menu = Menu::find($id);
            }
            return view('menu.crud', array('menu' => $menu, 'topmenus' => $topmenus, 'langs' => $langs));
        }

        public function delete($id = null)
        {
            $langs = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
            $topmenus = Menu::where('deleted', 'no')->get();
            $menu = Menu::find($id);
            return view('menu.crud', array('menu' => $menu, 'topmenus' => $topmenus, 'langs' => $langs));
        }

        public function save(Request $request)
        {
            //dd($request->input());
            $text = "";
            if ($request->crud == 'add') {
                $rules = array(
                    'name' => 'required|max:255',
                    'order' => 'numeric|min:1|max:100000'
                );
                $validator = Validator::make(Input::all(), $rules);
                if ($validator->fails()) {
                   return \Redirect::back()->withErrors($validator)->withInput();
                }
                $languages = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
                $menu = new Menu();
                if ($request->topmenu != 'null') { 
                    $menu->top_id = $request->topmenu; 
                }
                $menu->type = $request->type;
                $menu->description = $request->description;
                $menu->order = $request->order;
                $menu->position = $request->position.'';
                $menu->status = ($request->status == 'active') ? 'active' : 'passive';
                $menu->save();
                foreach ($languages  as $language) {
                    $menuVariable = new MenuVariable();
                    $menuVariable->menu_id = $menu->id;
                    $menuVariable->lang_code = $language->code;
                    $menuVariable->name = $request->name;
                    $menuVariable->slug = str_slug($request->name, '-');
                    $menuVariable->title = $request->title;
                    $menuVariable->save();
                }
                $text = 'Başarıyla Eklendi...';
            }else if($request->crud == 'edit'){
                if (is_null($request->lang)) {
                    $rules = array(
                        'order' => 'numeric|min:1|max:100000'
                    );
                    $validator = Validator::make(Input::all(), $rules);
                    if ($validator->fails()) {
                       return \Redirect::back()->withErrors($validator)->withInput();
                    }
                    $menu = Menu::find($request->menu_id);
                    //dd($request->topmenu);
                    if ($request->topmenu == 'null') { 
                        $menu->top_id = null; 
                    }else{
                        $menu->top_id = $request->topmenu; 
                    }
                    $menu->description = $request->description;
                    $menu->order = $request->order;
                    $menu->position = $request->position.'';
                    $menu->status = ($request->status == 'active') ? 'active' : 'passive';
                    if ($menu->type == 'link') {
                        $menu->headertheme = 'general';
                        $menu->slidertype = 'no';
                        $menu->breadcrumbvisible = 'no';
                        $menu->asidevisible = 'no';
                        $menu->listtype = null;
                    }else{
                        $menu->headertheme = $request->headertheme.'';
                        $menu->slidertype = $request->slidertype.'';
                        $menu->breadcrumbvisible = $request->breadcrumbvisible.'';
                        $menu->asidevisible = $request->asidevisible.'';
                        if ($menu->type == 'list' || $menu->type == 'photogallery') {
                            $menu->listtype = $request->listtype.'';
                        }else{
                            $menu->listtype = null;
                        }
                    }
                    $menu->save();
                }else{
                    //dd($request->input());
                    $rules = array(
                        'name' => 'required|max:255',
                    );
                    $validator = Validator::make(Input::all(), $rules);
                    if ($validator->fails()) {
                       return \Redirect::back()->withErrors($validator)->withInput();
                    }
                    $menuVariable = Menu::find($request->menu_id)->variableLang($request->lang);
                    if (!isset($menuVariable)) {
                        $menuVariable = new MenuVariable();
                        $menuVariable->menu_id = $request->menu_id;
                        $menuVariable->lang_code = $request->lang;
                    }
                    $menuVariable->name = $request->name;
                    $menuVariable->slug = str_slug($request->slug, '-');
                    $menuVariable->title = $request->title;
                    if ($menuVariable->menu->type == 'link') {
                        $menuVariable->stvalue = json_encode(['link' => $request->stvalue_link, 'target' => $request->stvalue_target]);
                    }
                    $menuVariable->save();
                }
                $text = 'Başarıyla Düzenlendi...';
            }else if($request->crud == 'delete'){
                $menu = Menu::find($request->menu_id);
                $menu->deleted = 'yes';
                $menu->status = 'passive';
                $menu->save();
                foreach ($menu->variables as $menuVariable) {
                    $menuVariable->slug = null;
                    $menuVariable->save();
                }
                $text = 'Başarıyla Silindi...';
            }
            return redirect('menu/list')->with('message', array('text' => $text, 'status' => 'success'));
        }
    /** MENÜ **/

    /** CONTENT **/
        public function content($id = null)
        {
            $langs = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
            $menu = Menu::find($id);
            return view('menu.content', array('menu' => $menu, 'langs' => $langs));
        }

        public function content_add_edit($id, $cid = null, $lang = null)
        {
            $langs = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
            $tags = Tag::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
            $categories = Category::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
            $menu = Menu::find($id);
            if(is_null($cid)){
                $content = new Content();
            }else{
                $content = Content::find($cid);
                if(!is_null($lang) && is_null($content->variableLang($lang))){
                    $contentVariable = new ContentVariable();
                    $contentVariable->content_id = $content->id;
                    $contentVariable->lang_code = $lang;
                    $contentVariable->title = '';
                    $contentVariable->save();
                }
            }
            return view('menu.contentcrud', array('menu' => $menu, 'content' => $content, 'langs' => $langs, 'tags' => $tags, 'categories' => $categories));
        }

        public function content_delete($id, $cid = null)
        {
            $langs = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
            $tags = Tag::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
            $categories = Category::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
            $menu = Menu::find($id);
            $content = Content::find($cid);
            return view('menu.contentcrud', array('menu' => $menu, 'content' => $content, 'langs' => $langs, 'tags' => $tags, 'categories' => $categories));
        }

        public function content_save(Request $request)
        {
            //dd($request->input());
            $text = "";
            if($request->crud == 'add'){
                $rules = array(
                    'title' => 'required|max:255',
                    'order' => 'numeric|min:1|max:100000'
                );
                $validator = Validator::make(Input::all(), $rules);
                if ($validator->fails()) {
                   return \Redirect::back()->withErrors($validator)->withInput();
                }
                $languages = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
                $content = new Content();
                //$content->menu_id = $request->menu_id;
                if ($request->type != 'null') {
                    $content->type = $request->type.'';
                }
                $content->order = $request->order;
                $content->status = ($request->status == 'active') ? 'active' : 'passive';
                $content->save();
                foreach ($languages as $language) {
                    $contentVariable = new ContentVariable();
                    $contentVariable->content_id = $content->id;
                    $contentVariable->lang_code = $language->code;
                    $contentVariable->title = $request->title;
                    $contentVariable->save();
                }
                $menu_has_content = new MenuHasContent();
                $menu_has_content->menu_id = $request->menu_id;
                $menu_has_content->content_id = $content->id;
                $menu_has_content->save();
                $text = 'Başarıyla Eklendi...';
                return redirect('menu/content/'.$request->menu_id)->with('message', array('text' => $text, 'status' => 'success'));
            }else if($request->crud == 'edit'){
                if (is_null($request->lang)) {
                    $rules = array(
                        'order' => 'numeric|min:1|max:100000'
                    );
                    $validator = Validator::make(Input::all(), $rules);
                    if ($validator->fails()) {
                       return \Redirect::back()->withErrors($validator)->withInput();
                    }
                    $languages = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();

                    $content = Content::find($request->content_id);
                    $content->order = $request->order;
                    $content->status = ($request->status == 'active') ? 'active' : 'passive';
                    $content->save();
                    
                    if ($content->type == 'text') {
                        foreach ($languages as $language) {
                            $contentVariable = Content::find($request->content_id)->variableLang($language->code);
                            if(is_null($contentVariable)){
                                $contentVariable = new ContentVariable();
                                $contentVariable->content_id = $request->content_id;
                                $contentVariable->lang_code = $language->code;
                                $contentVariable->title = '';
                                $contentVariable->save();
                            }
                        }
                    }

                    if ($content->type == 'photo') {
                        foreach ($languages as $language) {
                            $contentVariable = Content::find($request->content_id)->variableLang($language->code);
                            if(is_null($contentVariable)){
                                $contentVariable = new ContentVariable();
                                $contentVariable->content_id = $request->content_id;
                                $contentVariable->lang_code = $language->code;
                                $contentVariable->title = '';
                                $contentVariable->props = $request->props;
                                $contentVariable->save();
                            }else{
                                $contentVariable->props = $request->props;
                                $contentVariable->save();
                            }
                        }
                    }

                    if ($content->type == 'photogallery') {
                        foreach ($languages as $language) {
                            $contentVariable = Content::find($request->content_id)->variableLang($language->code);
                            if(is_null($contentVariable)){
                                $contentVariable = new ContentVariable();
                                $contentVariable->content_id = $request->content_id;
                                $contentVariable->lang_code = $language->code;
                                $contentVariable->title = '';
                                $contentVariable->props = $request->props;
                                $contentVariable->save();
                            }else{
                                $contentVariable->props = $request->props;
                                $contentVariable->save();
                            }                            
                        }
                    }

                    if ($content->type == 'link') {
                        foreach ($languages as $language) {
                            $contentVariable = Content::find($request->content_id)->variableLang($language->code);
                            if(is_null($contentVariable)){
                                $contentVariable = new ContentVariable();
                                $contentVariable->content_id = $request->content_id;
                                $contentVariable->lang_code = $language->code;
                                $contentVariable->title = '';
                                $contentVariable->save();
                            }
                        }
                    }

                    if ($content->type == 'form') {
                        foreach ($languages as $language) {
                            $contentVariable = Content::find($request->content_id)->variableLang($language->code);
                            if(is_null($contentVariable)){
                                $contentVariable = new ContentVariable();
                                $contentVariable->content_id = $request->content_id;
                                $contentVariable->lang_code = $language->code;
                                $contentVariable->title = '';
                                $contentVariable->save();
                            }
                        }
                    }

                    $tagItems = ContentHasTag::where('content_id', $request->content_id)->delete();
                    if (!is_null($request->tag)) {
                        foreach ($request->tag as $tt) {
                            $tagItem = new ContentHasTag();
                            $tagItem->content_id = $request->content_id;
                            $tagItem->tag_id = $tt;
                            $tagItem->save();
                        }
                    }

                    $categoryItems = ContentHasCategory::where('content_id', $request->content_id)->delete();
                    if (!is_null($request->category)) {
                        foreach ($request->category as $cc) {
                            $categoryItem = new ContentHasCategory();
                            $categoryItem->content_id = $request->content_id;
                            $categoryItem->category_id = $cc;
                            $categoryItem->save();
                        }
                    }

                    $text = 'Başarıyla Düzenlendi...';
                }else{
                    //dd($request->input());
                    $rules = array(
                        'title' => 'required|max:255'
                    );
                    $validator = Validator::make(Input::all(), $rules);
                    if ($validator->fails()) {
                       return \Redirect::back()->withErrors($validator)->withInput();
                    }

                    $content = Content::find($request->content_id);
                    $contentVariable = Content::find($request->content_id)->variableLang($request->lang);
                    $contentVariable->title = $request->title;

                    if ($content->type == 'text') {
                        $contentVariable->short_content = $request->short_content;
                        $contentVariable->content = $request->content;
                        $contentVariable->row = $request->row;
                        $contentVariable->height = $request->height;
                    }

                    if ($content->type == 'photo') {}

                    if ($content->type == 'photogallery') {}

                    if ($content->type == 'link') {
                        $contentVariable->content = $request->content;
                        $contentVariable->props = $request->props;
                    }

                    if ($content->type == 'form') {
                        $contentVariable->content = $request->formdata;
                    }

                    $contentVariable->save();
                    $text = 'Başarıyla Düzenlendi...';
                }
                return redirect('menu/content/'.$request->menu_id)->with('message', array('text' => $text, 'status' => 'success'));
            }else if($request->crud == 'delete'){

                MenuHasContent::where('menu_id', $request->menu_id)->where('content_id', $request->content_id)->delete();
                if(MenuHasContent::where('content_id', $request->content_id)->count() == 0){
                    $content = Content::find($request->content_id);
                    if (is_null($content->top_content)) {
                        $content->deleted = 'yes';
                        $content->status = 'passive';
                        $content->save();
                    }
                    $text = 'Başarıyla Silindi...';
                }

                return redirect('menu/content/'.$request->menu_id)->with('message', array('text' => $text, 'status' => 'success'));
            }
        }

        public function content_addexist($id, $cid = null)
        {
            $activeLang = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
            $menu = Menu::find($id);
            $content = Content::where('deleted', 'no')->where('status', 'active')->orderBy('type', 'desc')->orderBy('order', 'asc')->get();
            $form = Content::where('type', 'form')->where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
            return view('menu.contentaddexist', array('menu' => $menu, 'content' => $content, 'form' => $form, 'activeLang' => $activeLang));
        }

        public function content_save_exist(Request $request)
        {
            $text = "";
            if($request->crud == 'addexist'){
                $menu_has_content = new MenuHasContent();
                $menu_has_content->menu_id = $request->menu_id;
                $menu_has_content->content_id = $request->existcontent;
                $menu_has_content->save();
                $text = 'Başarıyla Eklendi...';
                return redirect('menu/content/'.$request->menu_id)->with('message', array('text' => $text, 'status' => 'success'));

            }else if($request->crud == 'delete'){

                $content = Content::find($request->id);
                $content->deleted = 'yes';
                $content->status = 'passive';
                $content->save();
                $text = 'Başarıyla Silindi...';
                return redirect('menu/content/'.$content->menu_id)->with('message', array('text' => $text, 'status' => 'success'));
                
            }
        }
    /** CONTENT **/

    /** FORM LIST-DETAIL **/
        public function form_list()
        {
            $langs = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
            return view('form.list', array('langs' => $langs));
        }
    /** FORM LIST-DETAIL **/

    /** ADD-ON **/
        public function addons($id, $cid)
        {
            $langs = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
            $menu = Menu::find($id);
            if(is_null($id)){
                $content = new Content();
            }else{
                $content = Content::find($cid);
            }
            //$photogalleryitem = PhotoGallery::find($fid);
            return view('menu.addons', array('menu' => $menu, 'content' => $content, 'langs' => $langs));
        }

        public function addon_add_edit($id, $cid, $aid = null, $lang = null)
        {
            $langs = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();

            $tags = Tag::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
            $categories = Category::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
            $menu = Menu::find($id);
            $content = Content::find($cid);

            if(is_null($aid)){
                $addon = new Content();
            }else{
                $addon = Content::find($aid);
                if(!is_null($lang) && is_null($addon->variableLang($lang))){
                    $addonVariable = new ContentVariable();
                    $addonVariable->content_id = $addon->id;
                    $addonVariable->lang_code = $lang;
                    $addonVariable->title = '';
                    $addonVariable->save();
                }
            }
            return view('menu.addoncrud', array('menu' => $menu, 'content' => $content, 'addon' => $addon, 'langs' => $langs, 'tags' => $tags, 'categories' => $categories));
        }

        public function addon_delete($id, $cid, $aid = null)
        {
            $langs = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
            $activeLang = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
            $tags = Tag::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
            $categories = Category::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
            $menu = Menu::find($id);
            $content = Content::find($cid);
            $addon = Content::find($aid);
            return view('menu.addoncrud', array('menu' => $menu, 'content' => $content, 'addon' => $addon, 'langs' => $langs, 'activeLang' => $activeLang, 'tags' => $tags, 'categories' => $categories));
        }

        public function addon_save(Request $request)
        {
            //dd($request->input());
            if($request->crud == 'add'){
                $rules = array(
                    'title' => 'required|max:255',
                    'order' => 'numeric|min:1|max:100000'
                );
                $validator = Validator::make(Input::all(), $rules);
                if ($validator->fails()) {
                   return \Redirect::back()->withErrors($validator)->withInput();
                }
                $languages = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
                $addon = new Content();
                $addon->top_content = $request->content_id;
                if ($request->type != 'null') {
                    $addon->type = $request->type.'';
                }
                $addon->order = $request->order;
                $addon->status = ($request->status == 'active') ? 'active' : 'passive';
                $addon->save();

                foreach ($languages as $language) {
                    $addonVariable = new ContentVariable();
                    $addonVariable->content_id = $addon->id;
                    $addonVariable->lang_code = $language->code;
                    $addonVariable->title = $request->title;
                    $addonVariable->save();
                }

                $text = 'Başarıyla Eklendi...';
                return redirect('menu/content/'.$request->menu_id.'/addons/'.$addon->top_content)->with('message', array('text' => $text, 'status' => 'success'));

            }elseif($request->crud == 'edit'){
                if (is_null($request->lang)) {
                    $rules = array(
                        'order' => 'numeric|min:1|max:100000'
                    );
                    $validator = Validator::make(Input::all(), $rules);
                    if ($validator->fails()) {
                       return \Redirect::back()->withErrors($validator)->withInput();
                    }
                    $languages = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();

                    $addon = Content::find($request->addon_id);
                    $addon->order = $request->order;
                    $addon->status = ($request->status == 'active') ? 'active' : 'passive';
                    $addon->save();

                    if ($addon->type == 'photogallery') {
                        foreach ($languages as $language) {
                            $addonVariable = Content::find($request->addon_id)->variableLang($language->code);
                            $addonVariable->props = $request->props;
                            $addonVariable->save();
                        }
                    }

                    $tagItems = ContentHasTag::where('content_id', $request->addon_id)->delete();
                    if (!is_null($request->tag)) {
                        foreach ($request->tag as $tt) {
                            $tagItem = new ContentHasTag();
                            $tagItem->content_id = $request->addon_id;
                            $tagItem->tag_id = $tt;
                            $tagItem->save();
                        }
                    }

                    $categoryItems = ContentHasCategory::where('content_id', $request->addon_id)->delete();
                    if (!is_null($request->category)) {
                        foreach ($request->category as $cc) {
                            $categoryItem = new ContentHasCategory();
                            $categoryItem->content_id = $request->addon_id;
                            $categoryItem->category_id = $cc;
                            $categoryItem->save();
                        }
                    }

                    $text = 'Başarıyla Düzenlendi...';
                }else{
                    $rules = array(
                        'title' => 'required|max:255'
                    );
                    $validator = Validator::make(Input::all(), $rules);
                    if ($validator->fails()) {
                       return \Redirect::back()->withErrors($validator)->withInput();
                    }
                    $addon = Content::find($request->addon_id);
                    $addonVariable = Content::find($request->addon_id)->variableLang($request->lang);
                    $addonVariable->title = $request->title;
                    if ($addon->type == 'photogallery') {
                        $addonVariable->props = $request->props;
                    }
                    if ($addon->type == 'text') {
                    }else{
                        $addonVariable->row = 'normal';
                        $addonVariable->height = null;
                    }
                    $addonVariable->save();
                    $text = 'Başarıyla Düzenlendi...';
                }
                return redirect('menu/content/'.$request->menu_id.'/addons/'.$addon->top_content)->with('message', array('text' => $text, 'status' => 'success'));
            
            }elseif($request->crud == 'delete'){
                //dd($request->input());

                //MenuHasContent::where('menu_id', $request->menu_id)->where('content_id', $request->addon_id)->delete();
                if(MenuHasContent::where('content_id', $request->addon_id)->count() == 0){
                    $addon = Content::find($request->addon_id);
                    $addon->deleted = 'yes';
                    $addon->status = 'passive';
                    $addon->save();
                    $text = 'Başarıyla Silindi...';
                }else{
                    $addon = Content::find($request->addon_id);
                    $addon->top_content = null;
                    $addon->save();
                    $text = 'Başarıyla Silindi...';
                }
                return redirect('menu/content/'.$request->menu_id.'/addons/'.$addon->top_content)->with('message', array('text' => $text, 'status' => 'success'));
            }
            return redirect('menu/content/'.$request->menu_id.'/addons/'.$addon->top_content)->with('message', array('text' => $text, 'status' => 'success'));
        }
    /** ADD-ON **/

    /** TAG **/
        public function tag()
        {
            $langs = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
            return view('menu.tag', array('langs' => $langs));
        }

        public function tag_add_edit($tid = null, $lang = null)
        {
            $langs = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
            if(is_null($tid)){
                $tag = new Tag();
            }else{
                $tag = Tag::find($tid);
            }
            return view('menu.tagcrud', array('tag' => $tag, 'langs' => $langs));
        }

        public function tag_delete($tid)
        {
            $langs = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
            $tag = Tag::find($tid);
            return view('menu.tagcrud', array('tag' => $tag, 'langs' => $langs));
        }

        public function tag_save(Request $request)
        {
            //dd($request->input());
            $text = "";
            if ($request->crud == 'add') {
                $rules = array(
                    'title' => 'required|max:255',
                    'order' => 'numeric|min:1|max:100000'
                );
                $validator = Validator::make(Input::all(), $rules);
                if ($validator->fails()) {
                   return \Redirect::back()->withErrors($validator)->withInput();
                }
                $languages = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
                $tag = new Tag();
                $tag->order = $request->order;
                $tag->status = ($request->status == 'active') ? 'active' : 'passive';
                $tag->save();
                foreach ($languages as $language) {
                    $tagVariable = new TagVariable();
                    $tagVariable->tag_id = $tag->id;
                    $tagVariable->lang_code = $language->code;
                    $tagVariable->title = $request->title;
                    $tagVariable->save();
                }
                $text = 'Başarıyla Eklendi...';
            }else if($request->crud == 'edit'){
                if (is_null($request->lang)) {
                    $rules = array(
                        'order' => 'numeric|min:1|max:100000'
                    );
                    $validator = Validator::make(Input::all(), $rules);
                    if ($validator->fails()) {
                       return \Redirect::back()->withErrors($validator)->withInput();
                    }
                    $tag = Tag::find($request->tag_id);
                    $tag->order = $request->order;
                    $tag->status = ($request->status == 'active') ? 'active' : 'passive';
                    $tag->save();
                }else{
                    //dd($request->input());
                    $rules = array(
                        'title' => 'required'
                    );
                    $validator = Validator::make(Input::all(), $rules);
                    if ($validator->fails()) {
                       return \Redirect::back()->withErrors($validator)->withInput();
                    }
                    $tagVariable = Tag::find($request->tag_id)->variableLang($request->lang);
                    if (!isset($tagVariable)) {
                        $tagVariable = new TagVariable();
                        $tagVariable->tag_id = $request->tag_id;
                        $tagVariable->lang_code = $request->lang;
                    }
                    $tagVariable->title = $request->title;
                    $tagVariable->save();
                }
                $text = 'Başarıyla Düzenlendi...';
            }else if($request->crud == 'delete'){
                $tag = Tag::find($request->tag_id);
                $tag->deleted = 'yes';
                $tag->status = 'passive';
                $tag->save();
                $text = 'Başarıyla Silindi...';
            }
            return redirect('tag')->with('message', array('text' => $text, 'status' => 'success'));
        }
    /** TAG **/

    /** CATEGORY **/
        public function category()
        {
            $langs = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
            return view('menu.category', array('langs' => $langs));
        }

        public function category_add_edit($cid = null, $lang = null)
        {
            $langs = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
            if(is_null($cid)){
                $category = new Category();
            }else{
                $category = Category::find($cid);
            }
            return view('menu.categorycrud', array('category' => $category, 'langs' => $langs));
        }

        public function category_delete($cid)
        {
            $langs = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
            $category = Category::find($cid);
            return view('menu.categorycrud', array('category' => $category, 'langs' => $langs));
        }

        public function category_save(Request $request)
        {
            //dd($request->input());
            $text = "";
            if ($request->crud == 'add') {
                $rules = array(
                    'title' => 'required|max:255',
                    'order' => 'numeric|min:1|max:100000'
                );
                $validator = Validator::make(Input::all(), $rules);
                if ($validator->fails()) {
                   return \Redirect::back()->withErrors($validator)->withInput();
                }
                $languages = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
                $category = new Category();
                $category->order = $request->order;
                $category->status = ($request->status == 'active') ? 'active' : 'passive';
                $category->save();
                foreach ($languages as $language) {
                    $categoryVariable = new CategoryVariable();
                    $categoryVariable->category_id = $category->id;
                    $categoryVariable->lang_code = $language->code;
                    $categoryVariable->title = $request->title;
                    $categoryVariable->save();
                }
                $text = 'Başarıyla Eklendi...';
            }else if($request->crud == 'edit'){
                if (is_null($request->lang)) {
                    $rules = array(
                        'order' => 'numeric|min:1|max:100000'
                    );
                    $validator = Validator::make(Input::all(), $rules);
                    if ($validator->fails()) {
                       return \Redirect::back()->withErrors($validator)->withInput();
                    }
                    $category = Category::find($request->category_id);
                    $category->order = $request->order;
                    $category->status = ($request->status == 'active') ? 'active' : 'passive';
                    $category->save();
                }else{
                    //dd($request->input());
                    $rules = array(
                        'title' => 'required'
                    );
                    $validator = Validator::make(Input::all(), $rules);
                    if ($validator->fails()) {
                       return \Redirect::back()->withErrors($validator)->withInput();
                    }
                    $categoryVariable = Category::find($request->category_id)->variableLang($request->lang);
                    if (!isset($categoryVariable)) {
                        $categoryVariable = new CategoryVariable();
                        $categoryVariable->category_id = $request->category_id;
                        $categoryVariable->lang_code = $request->lang;
                    }
                    $categoryVariable->title = $request->title;
                    $categoryVariable->save();
                }
                $text = 'Başarıyla Düzenlendi...';
            }else if($request->crud == 'delete'){
                $category = Category::find($request->category_id);
                $category->deleted = 'yes';
                $category->status = 'passive';
                $category->save();
                $text = 'Başarıyla Silindi...';
            }
            return redirect('category')->with('message', array('text' => $text, 'status' => 'success'));
        }
    /** CATEGORY **/

    /** PHOTO GALLERY **/
        public function photogalleryitem($id, $cid, $fid)
        {
            $activeLang = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
            $menu = Menu::find($id);
            if(is_null($id)){
                $content = new Content();
            }else{
                $content = Content::find($cid);
            }
            $photogalleryitem = PhotoGallery::find($fid);
            return view('menu.photogalleryitem', array('menu' => $menu, 'content' => $content, 'photogalleryitem' => $photogalleryitem, 'activeLang' => $activeLang));
        }
        
        public function photogalleryitem_delete($id, $cid, $fid)
        {
            $activeLang = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
            $menu = Menu::find($id);
            $content = Content::find($cid);
            $photogalleryitem = PhotoGallery::find($fid);
            return view('menu.photogalleryitem', array('menu' => $menu, 'content' => $content, 'photogalleryitem' => $photogalleryitem, 'activeLang' => $activeLang));
        }

        public function photogalleryitem_save(Request $request)
        {
            //dd($request->input());
            $text = "";
            if ($request->crud == 'photogallery_edit') {
                $rules = array(
                    'order' => 'numeric|min:1|max:100000'
                );
                $validator = Validator::make(Input::all(), $rules);
                if ($validator->fails()) {
                   return \Redirect::back()->withErrors($validator)->withInput();
                }
                if($request->crud == 'photogallery_edit'){

                    $photogalleryitem = PhotoGallery::find($request->photo_gallery_id);
                    $photogalleryitem->name = $request->name;
                    $photogalleryitem->description = $request->description;
                    $photogalleryitem->order = $request->order;
                    $photogalleryitem->status = ($request->status == 'active') ? 'active' : 'passive';
                    $photogalleryitem->save();
                    $text = 'Başarıyla Düzenlendi...';
                    
                }
            }else if($request->crud == 'photogallery_delete'){
                $photogalleryitem = PhotoGallery::find($request->photo_gallery_id);
                $photogalleryitem->deleted = 'yes';
                $photogalleryitem->status = 'passive';
                $photogalleryitem->save();
                $text = 'Başarıyla Silindi...';
            }
            return redirect('menu/content/'.$request->menu_id.'/galup/'.$request->content_id)->with('message', array('text' => $text, 'status' => 'success'));
        }
    /** PHOTO GALLERY **/

    /** ADDON PHOTO GALLERY **/
        public function addon_photogalleryitem($id, $cid, $aid, $fid)
        {
            $activeLang = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
            $menu = Menu::find($id);
            if(is_null($id)){
                $content = new Content();
            }else{
                $content = Content::find($cid);
            }
            $photogalleryitem = PhotoGallery::find($fid);
            return view('menu.photogalleryitem', array('menu' => $menu, 'content' => $content, 'photogalleryitem' => $photogalleryitem, 'activeLang' => $activeLang));
        }
        
        public function addon_photogalleryitem_delete($id, $cid, $aid, $fid)
        {
            $activeLang = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
            $menu = Menu::find($id);
            $content = Content::find($cid);
            $photogalleryitem = PhotoGallery::find($fid);
            return view('menu.photogalleryitem', array('menu' => $menu, 'content' => $content, 'photogalleryitem' => $photogalleryitem, 'activeLang' => $activeLang));
        }

        public function addon_photogalleryitem_save(Request $request)
        {
            $text = "";
            if ($request->crud == 'photogallery_edit') {
                $rules = array(
                    'order' => 'numeric|min:1|max:100000'
                );
                $validator = Validator::make(Input::all(), $rules);
                if ($validator->fails()) {
                   return \Redirect::back()->withErrors($validator)->withInput();
                }
                if($request->crud == 'photogallery_edit'){

                    $photogalleryitem = PhotoGallery::find($request->fid);
                    $photogalleryitem->name = $request->name;
                    $photogalleryitem->description = $request->description;
                    $photogalleryitem->order = $request->order;
                    $photogalleryitem->status = ($request->status == 'active') ? 'active' : 'passive';
                    $photogalleryitem->save();
                    $text = 'Başarıyla Düzenlendi...';
                    
                }
            }else if($request->crud == 'photogallery_delete'){
                $photogalleryitem = PhotoGallery::find($request->fid);
                $photogalleryitem->deleted = 'yes';
                $photogalleryitem->status = 'passive';
                $photogalleryitem->save();
                $text = 'Başarıyla Silindi...';
                return redirect('menu/content/'.$request->menu_id.'/galup/'.$request->id)->with('message', array('text' => $text, 'status' => 'success'));
            }
            return redirect('menu/content/'.$request->menu_id.'/galup/'.$request->id)->with('message', array('text' => $text, 'status' => 'success'));
        }
    /** ADDON PHOTO GALLERY **/

    /** SLIDER **/
        public function stslider($id = null)
        {
            $langs = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
            $topmenus = Menu::where('deleted', 'no')->get();
            $menu = Menu::find($id);
            return view('menu.stslider', array('menu' => $menu, 'topmenus' => $topmenus, 'langs' => $langs));
        }

        public function stslider_add_edit($id, $sid = null)
        {
            $langs = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
            $menu = Menu::find($id);
            if(is_null($id)){
                $slide = new Slider();
            }else{
                $slide = Slider::find($sid);
            }
            return view('menu.stslidercrud', array('menu' => $menu, 'slide' => $slide, 'langs' => $langs));
        }

        public function stslider_delete($id, $sid = null)
        {
            $langs = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
            $menu = Menu::find($id);
            if(is_null($id)){
                $slide = new Slider();
            }else{
                $slide = Slider::find($sid);
            }
            return view('menu.stslidercrud', array('menu' => $menu, 'slide' => $slide, 'langs' => $langs));
        }

        public function stslider_save(Request $request)
        {
            $text = "";
            if($request->crud == 'add'){
                
                $rules = array(
                    'title' => 'required|max:255',
                    'order' => 'numeric|min:1|max:100000'
                );
                $validator = Validator::make(Input::all(), $rules);
                if ($validator->fails()) {
                   return \Redirect::back()->withErrors($validator)->withInput();
                }
                
                $languages = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();

                $slide = new Slider();
                $slide->menu_id = $request->menu_id;
                $slide->order = $request->order;
                $slide->status = ($request->status == 'active') ? 'active' : 'passive';
                $slide->save();

                foreach ($languages as $language) {
                    $slideVariable = new SliderVariable();
                    $slideVariable->slider_id = $slide->id;
                    $slideVariable->lang_code = $language->code;
                    $slideVariable->title = $request->title;
                    $slideVariable->description = $request->description;
                    $slideVariable->button_text = $request->button_text;
                    $slideVariable->button_url = $request->button_url;
                    $slideVariable->save();
                }

                $text = 'Başarıyla Eklendi...';
                return redirect('menu/stslider/'.$slide->menu_id)->with('message', array('text' => $text, 'status' => 'success'));

            }else if($request->crud == 'edit'){

                if (is_null($request->lang)) {

                    $rules = array(
                        'order' => 'numeric|min:1|max:100000'
                    );
                    $validator = Validator::make(Input::all(), $rules);
                    if ($validator->fails()) {
                       return \Redirect::back()->withErrors($validator)->withInput();
                    }

                    $slide = Slider::where('id', $request->slider_id)->first();
                    $slide->order = $request->order;
                    $slide->status = ($request->status == 'active') ? 'active' : 'passive';
                    $slide->save();

                }else{

                    $rules = array(
                        'title' => 'required|max:255'
                    );
                    $validator = Validator::make(Input::all(), $rules);
                    if ($validator->fails()) {
                       return \Redirect::back()->withErrors($validator)->withInput();
                    }

                    $slide = Slider::where('id', $request->slider_id)->first();
                    $slideVariable = $slide->variableLang($request->lang);
                    $slideVariable->title = $request->title;
                    $slideVariable->description = $request->description;
                    $slideVariable->button_text = $request->button_text;
                    $slideVariable->button_url = $request->button_url;
                    $slideVariable->save();

                }
                return redirect('menu/stslider/'.$slide->menu_id)->with('message', array('text' => $text, 'status' => 'success'));

            }else if($request->crud == 'delete'){

                $slide = Slider::find($request->slider_id);
                $slide->deleted = 'yes';
                $slide->status = 'passive';
                $slide->save();
                $text = 'Başarıyla Silindi...';
                return redirect('menu/stslider/'.$slide->menu_id)->with('message', array('text' => $text, 'status' => 'success'));

            }
        }
    /** SLIDER **/

    public function stimage($id)
    {
        $langs = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
        $topmenus = Menu::where('deleted', 'no')->get();
        $menu = Menu::find($id);
        return view('menu.stimage', array('menu' => $menu, 'topmenus' => $topmenus, 'langs' => $langs));
    }
}
